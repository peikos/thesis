SUBS = initial_proposal phase_one thesis presentations/lect_start presentations/lect_update
PDFS = $(addprefix public/, $(SUBS:=.pdf))
INTERMEDIATE = $(addsuffix /main.pdf, $(SUBS))

.PHONY: all

all: .githead public $(PDFS) $(INTERMEDIATE)

pdf_figures: figures
	$(MAKE) -C figures

%/main.pdf: % commit.tex pdf_figures
	$(MAKE) -C $<

public/%.pdf: %/main.pdf
	cp $< $@

public: index.html default.css CHANGELOG
	mkdir -p $@/presentations
	cp $^ $@

.githead:
	@echo generating .githead; \
	if [ -f .git/refs/heads/main ] ; then \
		echo ".head: .git/refs/heads/main" >> .githead; \
	elif [ -f .git ]; then \
	echo "$(subst gitdir, .head, $(shell cat .git))/refs/heads/main" >> .githead; \
	else \
		echo ".head:" >> .githead; \
	fi; \
	echo "	touch .head" >> .githead

CHANGELOG: .head
	git log --pretty="format:%h %B" --abbrev-commit > CHANGELOG
	echo >> CHANGELOG
	PROJ_ROOT=$$PWD; for DIR in $(SUBS); do\
		echo \# Document $$DIR.pdf: >> CHANGELOG;\
		cd $$DIR;\
		texcount -merge main.tex >> $$PROJ_ROOT/CHANGELOG;\
		cd -;\
	done

commit.tex: .head
	git log -n 1 --pretty=format:'%h (%cD)\\%s %b' > $@

clean:
	rm -rf *.aux *.bbl *.bcf *.blg *.log *.run.xml *.out *.toc CHANGELOG commit.tex public *.pdf .githead .head
	$(MAKE) clean -C figures
	for DIR in $(SUBS); do\
		$(MAKE) clean -C $$DIR;\
	done

-include .githead
