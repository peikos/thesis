\chapter{Research Methodology}\label{chap:methodology}

This chapter describes and motivates the methodology used in this research project, its objectives, and the central research questions.

\newthought{The primary methodological framework} used for this research project follows the principles of Design Science Research, the application of which to information science was posited by Hevner\cite{hevnerDesignScienceInformation} in  2004. In this approach, the central process consists of of a practical problem, which is then to be progressively solved by the creation of innovative artefacts. This design phase is informed by interpretation on the desires as formulated by the stakeholders and study of existing work in relevant fields. At the same time, the results of this design phase are continuously evaluated, providing further direction for the cycle to repeat with the refinement of existing or creation of new artefacts\cite{johannessonIntroductionDesignScience2014}. This method matches the objective of this project, where the desired outcome is the creation, study and adoption of a novel solution, rather than the study of an already present phenomenon or framework. The focus, then, is twofold: On one hand, study of existing methods and previous research is an integral part of this research. On the other hand, no batteries-included solution is readily available, so innovative exloration and recombination of existing techniques will also be necessary to solve the issues at hand. Using these two approaches, it is the intention of this project to explore the design and evaluation of a possible conceptual model to tackle the issue. The focus herein is on research, to evaluate whether and why the chosen model is applicable, rather than producing a finished, ready-to-market product.

For the scope of this project, the problem to be addressed is that of identifying sentiment, specifically incitement, from short public social media posts such as tweets.
The problem arises from the fact that existing methods of sentiment analysis depend on a certain minimum amount of content in order to correctly predict the general sentiment of a text. Most methods are based on a bag-of-words model, wherein stop words\footnote{generally short common words with little or no semantic content, instead providing syntactic information on how other words relate.} are removed leaving even less text to work with.
A tweet, by definition, is short, and thus on itself may fail to provide adequate textual information for the detection of sentiment. This is further complicated by the fact that tweets are generally comprised of informal language, and can include a large amount of information not recognised as text by naive natural language processing: hashtags, accidental or delibarate misspellings, links, emoji, etc. This leaves the amount of parseable text generally even lower than the 280 character limit imposed by Twitter, and causes every rejected word to have a relatively large impact.

To apply the design science approach to this project, the three cycles of Hevner\cite{hevnerThreeCycleView2007} are used as a guideline. The central design phase cycles between the construction of artefacts to test out theories, and using the results to refine the assumptions for the next cycle.

\section{Research Question}\label{sec:rq}
In order to structure and scope this project, a main research question has been formulated to translate the relevant (as determined by the scope of this sub-project) parts of the eventual desired outcome --- a prototype AI able to determine sentiment --- into a research oriented project. This project is aimed at providing not only a prototype model for capturing intent, but also demonstrating its adequacy and the constraints placed on its responsible real-world application by the nature of the data used to feed the model. Given this, the main research question is posed as follows:\\[4mm]

\indent\emph{How can a sufficiently complete model be designed to capture intent from series of short informal text messages with minimal redundancy, in such a way as to be applicable to responsibly predict incitement based on their conversational structure?   }\\[4mm]

\section{Subquestions}
\noindent This research follows three separate but codependent phases, each principally attached to one of the cycles as described by Hevner: \begin{inparaenum} \item Collecting domain knowledge and formulating a model artefact [rigour/design cycle], \item Verification of the artefact [rigour cycle], and \item Application context and concerns [relevance cycle] \end{inparaenum} The rest of this section will discuss each cycle in more detail and formulate the relevant subquestions.
%Following this, a summary is provided including a graphical representation of the questions and their dependencies in Figure~\ref{fig:idg}.


\subsection{Model Design and Architecture}

  \newthought{ \textsf{Ri1}: \enquote{What indicators from discourse pragmatics and sentiment analysis are applicable in the detection of incitement and categorisation of intent?} }\\[2mm]

  \noindent This question is intended to get a grounded overview of applicable indicators in the domain of discourse pragmatics pertaining to sentiment, and in the domain of sentiment analysis pertaining to conversations, in order to detect emotion --- specifically incitement. This question will be used to fuel the design process required to answer \textsf{De1} and will be part of the literature review for this project. Relevant papers will include one or more of the following:
    \begin{itemize}
    \item The application of sentiment analysis as regarding to the detection of incitement or negative emotional content.
    \item The application of discourse pragmatics as regarding the evolution of emotional content within conversations.
    \end{itemize}

  \newthought{ \textsf{De1}: \enquote{How can these indicators be translated into a model able to capture intent? } }\\[2mm]

  \noindent This question is central to the design cycle of this research, as its results describe the main artefact produced in this research project.

  The answers to these questions, as well as a more detailed description of the factors forcing the inclusion of this extra question and the considerations taken into account in the process of answering it will be described in Chapter~\ref{chap:model}.

\subsection{Data Acquisition and Analysis}

  The next set of questions are intended to verify the results of the main design cycle using rigour-based techniques of statistical analysis. These are meant to ensure the coverage and lack of redundancy required in the main research question.

  \newthought{ \textsf{Ri2}: \enquote{What is the coverage of the acquired dataset with regard to the entire possible space of intentions as determined by the chosen indicators?} }\\[2mm]

  \noindent Here, the aim is to identify clusters within the space (again, using algorithms such as t-SNE) to determine the completeness of the data represented. The goal is to identify empty regions, attempt to explain why these gaps show up, and whether these represent gaps in the degree that the dataset represents relevant Twitter discourse.

  \todo{This question should concern more with the model than with the specific dataset used to analyse the model. It is a prerequisite to answer the following question, although the way it is answered depends more on the specific data than the next question.}

  \newthought{ \textsf{Ri3}: \enquote{What is the level of relevance of the indicators identified in (Ri1) in capturing intent information of tweets and providing explainability?} }\\[2mm]

  \noindent This question addresses the choices made in the design of the intent-space and its chosen basis vectors (axes). This question aims to determine how well these bases where chosen, and how the computational usability of the dataset could (if and when the usage warrants it) be increased without sacrificing the content present. To answer this question, explanatory factor analysis and the t-SNE cluster-preserving dimensionality-reduction algorithm are utilised. The goal is to determine to what extend dimensionality reduction can be applied without damaging the data contained in the set, and preferably whilst maintaining explainable labels for the remaining axes.

These questions together aim to verify the choices made in the first phase of this project by subjecting the assumptions to real-world data. The answers to these questions, as well as a more detailed description of the factors forcing the inclusion of this extra question and the considerations taken into account in the process of answering it will be described in Chapter~\ref{chap:data}.

\subsection{Application Concerns}
  The last two questions deal with the implications of the results of this research, which precautions should be considered in its application, and how to interpret results when using the model in prediction tasks.

  \newthought{ \textsf{Re1}: \enquote{What ethical { \color{red} pitfalls } can be anticipated in the application of the design artefact?} }\\[2mm]

  \noindent This question aims to place the model designed in this research project in the broader context of its application. As with all AI and data based projects, ethical considerations form a major concern informing how the results should be interpreted and applied to real-world situations.

  \newthought{ \textsf{Re2}: \enquote{To what degree can bias be identified in the dataset, and how has this been introduced by the tagging process used in its construction?} }\\[2mm]

  \noindent This question is twofold, as the second part only makes sense to ask if substantial bias can be shown to be present. The main goal of this question is to determine how biased (and biased how) the tags used to determine the embeddings of tweets within the space are. The approach here is to analyse the (co)variances on each axis within groups and compare this to the (co)variances between groups using methods such as Student's t-test. This question aims to identify and isolate these biases insofar as these can be correlated to known differences in the backgrounds of the taggers, in order to determine whether (and in what way) factors such as educational background (social vs applied exact science) colour the resulting tags and whether the average that determines the final embeddings should be weighed to better reflect a more general or specialised population.

These questions together aim to address the responsibility constraint present in the main research question. The answers to these questions, as well as a more detailed description of the factors forcing the inclusion of this extra question and the considerations taken into account in the process of answering it will be described in Chapter~\ref{chap:relevance}.




\subsection{Summary}
In summary, the project has been divided into three relevant sub-problems, each represented in the rigour- and design cycles. These three sub-problems correspond to the two domains of knowledge relevant for the case at hand, supplemented with the need for benchmarking the results. The relevance cycle is less substantive for this project, as the results will be unlikely to be adopted in time for the duration of this research project. Consequently, this cycle will be limited to a consideration of the ethical implications of adopting an AI system as considered in this study. The internal dependencies of the total project are visualised in Figure~\ref{fig:idg}.
%\begin{figure}[h]
%\centering
%\begin{tikzpicture}
     %\node[] (ri1)         {\sf Ri1};
     %\node[below=of ri1] (ri2)         {\sf Ri2};
     %\node[below=of ri2] (ri3) {\sf Ri3};
     %\node[right=2 cm of ri1] (de1)         {\sf De1};
     %\node[below=of de1] (de2)         {\sf De2};
     %\node[below=of de2] (de3) {\sf De3};
     %\node[right=2 cm of de1] (re1)         {\sf Re1};
     %\node[right=2 cm of de2] (re2)         {\sf Re2};
     %\draw[dotted] (-0.7,-4) rectangle (0.8,1);
     %\draw[dotted] (2.1,-4) rectangle (3.5,1);
     %\draw[dotted] (4.9,-4) rectangle (6.3,1);
     %\draw (-1.2,1.8) rectangle (1.2,2.3);
     %\draw (1.6,1.8) rectangle (4.0,2.3);
     %\draw (4.2,1.8) rectangle (7.0,2.3);
     %\node[above=15mm of ri1] (rigour)         {\bf Rigour Cycle};
     %\node[above=15mm of de1] (design)         {\bf Design Cycle};
     %\node[above=15mm of re1] (relevance)         {\bf Relevance Cycle};
     %\draw[-latex, dotted] (ri1) edge[out=25,  in=155] (de1);
     %\draw[-latex, dotted] (de1) edge[out=205, in=335] (ri1);
     %\draw[-latex, dotted] (ri2) edge[out=25,  in=155] (de2);
     %\draw[-latex, dotted] (de2) edge[out=205, in=335] (ri2);
     %\draw[-latex, dotted] (ri3) edge[out=25,  in=155] (de3);
     %\draw[-latex, dotted] (de3) edge[out=205, in=335] (ri3);
     %\draw[-latex, dotted] (re1) edge (re2);
     %\node (a) at (2.9,2.4)       {};
     %\node (b) at (5.5,2.4)       {};
     %\node (c) at (5.5,1.7)       {};
     %\node (d) at (2.9,1.7)       {};
     %\node (e) at (0.1,2.4)       {};
     %\node (f) at (2.7,2.4)       {};
     %\node (g) at (2.7,1.7)       {};
     %\node (h) at (0.1,1.7)       {};
     %\draw[-latex, line width=1.0mm] (a) edge[out=25, in=155] (b);
     %\draw[-latex, line width=1.0mm] (c) edge[out=205, in=335] (d);
     %\draw[-latex, line width=1.0mm] (e) edge[out=25, in=155] (f);
     %\draw[-latex, line width=1.0mm] (g) edge[out=205, in=335] (h);
     %\draw[dotted] (rigour) -- +(0,-1cm);
     %\draw[dotted] (design) -- +(0,-1cm);
     %\draw[dotted] (relevance) -- +(0,-1cm);
  %\end{tikzpicture}
%\caption{Internal Dependency Graph}\label{fig:idg}
%\end{figure}
