\chapter{Background}\label{chap:bg}

This chapter contains a broad overview of the theoretical basis of this research, starting with the definitions of some central subject: Utterances and incitement. It will subsequently explore some related  work on analysis of Twitter data, which mainly focuses on sentiment analysis, and briefly discuss why this approach falls short for the matter of interest of this research.


  % Introductie begint lezervriendelijk anecdotisch waar gaat het over / hoe zit het? Probleem vertellend in neerzetten

  % Daarna in background context geven
  % - voor wie opdracht
  % - wat is de opdracht
  % - wat wil je bereiken
  % - wat zijn de mogelijke problemen
  %
  % Wat zijn de deliverables, wat beoog je binnen project op de leveren
  %
  %
  % Background: definities - wat zijn talige elementen, wat is incitement, definities TK, significantie
  %  SIGNIF: Er is al iets gedaan over sentiment analyse, er is al iets gedaan over structuur in twitter, wij gaan dit combineren - maken gebruik van rijkheid in wetenschap die er al is.

\section{Definitions}
This section provides some definitions for terms that will be used extensively in the remainder of this text.

\subsection{Tweets, Utterances, and Discourse}
For the purpose of analysing text, the first thing that must be agreed upon is which unit of language to consider central. In this research, the most obvious choice for this would be a single tweet. A tweet can be seen as a unit of language which can be subdivided into smaller units such as sentences, hashtags, etc. These subdivisions can be analysed for sentiment and intent, but the overall intent of the tweet is central to consideration. On the other hand, multiple tweets together can form a conversation, which can also be attributed sentiment and intent. This level will be considered as a central part in this research, but the intent of a conversation is determined by the intent of its constituent tweets. The reason to define the tweet and not the conversation as central is because the tweet is the largest unit in which we can be reasonably certain that the content reflects the sentiment and intent of a single author. By combining multiple tweets into a conversation, the resulting entity \emph{can} have a dominant intent behind it, but this is unlikely to be necessarily the case.

\newthought{The remainder of this chapter} explores existing fields of knowledge dealing with analysing text on different levels and with different goals. Though some prior work has been done on tweets in particular, the vast majority of scientific writing predates Twitter and as such considers other units of text. The unit most similar to a tweet appears to be that of a \emph{discourse}, in that it can be seen as a building block of conversation. The concept of discource originates in the field of pragmatics, which together with discourse analysis is explored in Section~\ref{ssec:prag}. Fetzer\cite{fetzerConceptualisingDiscourse2014} describes discourse as being built up from sentences or \emph{utterances}. Despite the prevalence of the latter term, most authors refrain from providing an exact definition and presume reader familiarity. Levinson\cite{levinsonPragmatics1983} attemps to do so by juxtaposing the concept of utterance with the concept of sentence. He notes that the sentence is defined based on grammatical considerations, whereas the concept of utterance resides in the uttering of a sentence within a context. Multiple utterances together can form the aforementioned discourse, which in turn forms the building blocks of conversation. Framing the concept of discourse in utterances in lieu of sentences is more relevant in the context of this research, due to the nature of tweets as a digital equivalent to spoken text and will thus be preferred. The term \emph{sentence} will be used purely in a grammatical sense when discussing from a semantic of syntactic perspective.

\subsection{Incitement}
In order to be able to detect incitement in tweets, or any medium in general, the first requirement would be a usable definition of the concept. Timmermann\cite{timmermannIncitementInternationalLaw2014} spends the first chapter of \emph{Incitement in International Law} on providing a definition, which he summarises as generally including five elements:
\begin{pquotation}{Timmermann, 2014}
\begin{enumerate}
  \item{Negative stereotyping of the target group.}
  \item{Characterization of the target group as an extreme threat.}
  \item{Advocacy for an \enquote{eliminationist} or discriminatory solution to the perceived threat in the sense of excluding the target group members from society or the human community.}
  \item{The incitement is carried out in public.}
  \item{The incitement is part of a particular context which dramatically increases the effectiveness of the inciting words, usually through the involvement of the State or another powerful organization.}
\end{enumerate}
\end{pquotation}

It is not stated outright whether all of these should be present to constitute incitement, or whether certain combinations are valid on their own. The indicators quoted above are listed as \enquote{general components} of incitement to hatred. Similarly, the author does not put any clear requirements on the medium used for the delivery of incitement, and can thus be taken to refer to spoken or written text, images, etc. For the purpose of describing incitement within the context of international law it stands to reason not to limit the definition to a specific medium as new forms of communication can arise and should be automatically included if the relevant indicators are present. As an example, laws on content and (the limits of) freedom of speech have been around for longer than the medium of video-games, but the same rules should generally apply as for other forms of expression, insofar as this makes sense for the new medium. Conversely, for the purpose of this research, a definition on incitement should focus on the more narrow scope of the project: Twitter and similar forms of communication. This difference is reflected in the definition provided below by considering discourse and utterances.

\newthought{Returning to the indicators} provided by Timmermann, the third item appears to be the most relevant to this study as it captures a \emph{call to action} which suggests imminent violence against a targeted group --- a situation that a newsroom should be able to react to in short order. It therefore stands to reason to view the presence of this indicator, even in isolation, as a priority in detecting incitement.

Of the other indicators, the first and second describe what could be considered hate-mongering, but these indicators alone lack the call to action that warrants immediate response by authorities. This missing aspect could follow from the context: if a user is known to advocate violence against group A and in a later tweet compares group B to group A, this can be viewed as a call to violence against group B as well. The first two indicators, therefore, should not be dismissed entirely, but in isolation do not warrant the level of scrutiny as discourse including a direct call to action.

The fourth, and to a lesser extent the fifth of Timmermann's indicators are more of a given in the context, as tweets are by definition\footnote{It is possible to put a Twitter profile on private, thus removing the public aspect of its tweets. Tweets on a private profile cannot be seen by the general public, which will also result in those tweets not being visible to newsroom analysts or any potential AI-based solutions. This is per design of Twitter and therefore private tweets are left outside the scope of this research.} public and the amplifying context is provided by the platform and the people on it reading the tweets. Still, the amount of followers a user has, or more generally the projected reach the tweet has can be considered a relevant aspect within the spirit of this element of the definition.

\begin{definition}[Incitement]
  Discourse or utterance implying or advocating hostile action against a demonised person, people or status quo.
\end{definition}

\subsection{Additional Definitions}
The following terms are used in the following chapter discussing the research methodology. These terms generally refer to entire areas of study, the relevant parts of which will be further explored when they arise. This section provides a rough definition on some unfamiliar terms so that these may be used in formulating research questions and methodological discussion.

\paragraph{Semantics} Within the context of linguistics and philosophy, semantics refers to the study of meaning and truth assigned to words and sentences.

\paragraph{Discourse Pragmatics} The two related areas of study \emph{Discourse Analysis} and \emph{Pragmatics} are often used interchangeably to refer to the study of meaning of natural language beyond literal semantic meaning by regarding context and the way multiple sentences may be combined to evoke meaning beside their individual contents.

\todo{Add some references for basic definitions - SEP}

\section{Significance and Related Work}
A lot of work has gone into the application of sentiment analysis to Twitter data.
This includes procedures to work with noisy data\cite{barbosaRobustSentimentDetection2010, berminghamClassifyingSentimentMicroblogs2010} and emoji\cite{goTwitterSentimentClassification2009}\cite{readUsingEmoticonsReduce2005}. Also of potential interest is the work of Gonz\'{a}lez-Ib\'{a}nez, Muresan and Wacholder\cite{gonzalez-ibanezIdentifyingSarcasmTwitter2011} on identifying sarcasm in Twitter, as this may also be used to code subtext into tweets.
Additionally, Nazir, Ghaznafar, Maqsood, Aadil, Rho and Mehmood\cite{nazirSocialMediaSignal2019} investigate the combination of tweet volume, hashtags and sentiment analysis to perform signal detection.

\newthought{Mukherjee and Bhattacharyya}\cite{mukherjeeSentimentAnalysisTwitter} propose a method for polarity detection of tweets using discourse relations. Their work focusses on discourse relations within tweets, considering the tweet as a whole instead of sentences, but not considering conversations consisting of multiple tweets by different authors. The influence of discourse analysis on their work is the consideration of relations between sentences within a tweet, by considering conjunctions signifying coherence relations. This also serves to highlight a different approach to stop words from most semantics oriented work: Conjunctions are generally regarded as carrying no semantic information and thus discarded, but are here considered on a supra-semantic level.

Finally, Oluoch\cite{oluochSentimentAnalysisModel2020} in his masters thesis has studied the application of sentiment analysis for the detection of radicalisation on Twitter. The project follows a fairly standard approach of text classification machine learning approaches and does not consider the syntactic structure of tweets, nor the aspect of conversational flow.

\subsection{Sentiment versus Intent}
Generally, the goal of the work cited above gravitates to determining whether a tweet is positive (happy or excited about a product, person or situation) or negative (sad, angry or less than enthusiastic), which is subtely different from the concept of intent. The intent of any form of communication can be considered beneficious/constructive or malicious (signifying potential incitement) regardless of positivity or negativity. Consider four example utterances corresponding to the four possible combinations\footnote{
\begin{tabular}{l|l|l}
  & \bf positive & \bf negative \\
  \hline
  \bf beneficial & i & ii \\
  \hline
  \bf malicious & iii & iv \\
\end{tabular}
}:
\begin{enumerate}
  \item \enquote{I am happy to live in a society were healthcare is accessible!}
  \item \enquote{Utterly dismayed at recent developments, I hope they'll manage to fix this soon!}
  \item \enquote{This politician sucks, and something should be done about him!}
  \item \enquote{Today is a good day to die! We will bathe in the blood of our enemies!}
\end{enumerate}

In this example, (i) is both positive in sentiment and constructive; it should not register as incitement. Example (ii), whilst negative, does not express any incitement. The latter two examples do contain inflamatory language and as such should be flagged, despite (iii) sounding more negative and (iv) being likely to be flagged as positive based on the semantics of the most obvious indicator words. This distinction between sentiment and intent is important when determining whether and how to apply previous solutions to different problems to the subject of this research.

\section{Summary}
Based on a thorough search of available literature, the problem central to this research has not been solved in this form but related work is available to inform individual steps in the process of detecting incitement from tweets.
The fact that previous research on Twitter data mainly focuses on sentiment than intent does not invalidate this previously work for the context of this research. Whilst the indicators utilised may not be directly applicable, they can provide insight regardless of what information needs to be recovered from messages and how to deal with noise present in the medium. Previous work on computational semantics provides a basis to work with the meaning of the text within a tweet, whereas concepts from discourse pragmatics can be used to inform how to combine intent gathered from individual sentences to the level of tweets and conversations.
