\chapter{Model Design and Architecture}\label{chap:model}

This chapter details the results of the first hybrid rigour / design cycle. It elaborates the research done informing the choices made in the design of the artefact for this project. It will answer the first two research questions, \textsf{Ri1} and \textsf{De1}.

\section{Theoretical Review}

\subsection{Computational Semantics}
In order to make claims about whether an utterance contains incitement, we need be able to reason about the meaning or content of natural language. The study of meaning in general is known as \emph{semantics}\cite{barbarah.parteeSemantics1999}, and various theories exist within this domain using different formalisms to capture semantic content. The study of \emph{computational semantics}\cite{blackburnComputationalSemantics2003} specifically works with approaches applicable to automated processing, considering representations of meaning usable for computers. As the goals of this project lie in an AI based solution, computational semantics are considered as a starting point. Within this field, two broad approaches are explored for application in this study: \emph{Distributional semantics} and \emph{compositional semantics}. The former of these is explored in the following section, as the latter ultimately did not appear in the solutions explored in this research project.

\subsection{Distributional Semantics}
Translating the concept of semantics to computers and AI is challenging: The language used in general purpose computers is one of numbers\footnote{In essence, these numbers are binary integers. Using clever encodings, floating point numbers can be worked with as well.}, where no real equivalents exists for most real word concepts.
The field of \emph{distributional semantics}\cite{schutzeAutomaticWordSense} aims to derive meaning from the statistics of word cooccurrence and encode this information in terms of \emph{word embeddings}\cite{Jurafsky:2009:SLP:1214993}: vectors, which are essentially ordered lists of numbers.

Using a single number, values on a single scale can be given meaning; adding a second number provides a two-dimensional space in which meanings can be assigned to words. For example, Figure~\ref{fig:wespace} shows one possible way to encode a set of animals in a 2-dimensional semantic space. Whilst there are infinitely many possible ways to assign or \emph{embed} these points in the space, the chosen embedding is not arbitrary\cite{Jurafsky:2009:SLP:1214993}. Given the positions of the words and our knowledge of the animals, we could interpret the $x$ axis as representing average size, and the $y$ axis as a subjective measure of pettability.

\begin{figure}[h]
  \begin{tikzpicture}
     \draw (0,0) rectangle (5,5);

     \node (dog) at (2.6, 4.7) {dog};
     \fill (3.0, 4.5) circle (0.05);

     \node (cat) at (2.85, 4.1) {cat};
     \fill (2.5, 4.2) circle (0.05);

     \node (hamster) at (2.25, 3.8) {hamster};
     \fill (1.5, 4.0) circle (0.05);

     \node (ant) at (0.8, 0.7) {fire ant};
     \fill (0.8, 0.5) circle (0.05);

     \node (elephant) at (4.0, 1.7) {elephant};
     \fill (4.0, 1.5) circle (0.05);

     \node (whale) at (4.5, 0.8) {whale};
     \fill (4.5, 1.0) circle (0.05);

     \node (size) at (0.7, -0.3) {\it size $\to$};
     \node[rotate=90] (pet) at (-0.3, 1.2) {\it pettability $\to$};
  \end{tikzpicture}
\caption{An example 2D word embedding space}\label{fig:wespace}
  \end{figure}

The result of the informed embedding given in the example is that words assigned closely together represent words with some similarity in semantic context. Pets, in the example in Figure~\ref{fig:wespace}, are clustered towards the top (highly pettable animals) and mainly centred on the $x$ axis (not too large or too small to keep around). The example here is limited, but serves to illustrate a central point in distributional semantics: linguistic items with similar embeddings have similar meanings. By adding additional axes, more semantic context can be encoded.

To encapsulate the complexity of human language semantics, the number of dimensions required is generally in the order of magnitude of a few hundred axes\cite{Jurafsky:2009:SLP:1214993}. In general, greater dimensionality allows for greater granularity in expressing the meaning of terms and the space available for relational connections. At the same time, high dimensionality incurs a computational cost which gives rise to a tradeoff.

The vector space model for semantics is powerful in that it allows computers to reason about the semantics of words using vector arithmetic operations\cite{baroniNounsAreVectors2010}, and as such will be referenced in other research areas going forward.

\newthought{As each word encountered in a text} needs an associated vector, and each vector requires a large amount of numeric values, assigning these embeddings manually is unfeasible, nor will randomly assigned vectors work in providing the desired semantic context. The field of distributive semantics therefore also deals with methods to automatically generate such vector spaces\cite{almeidaWordEmbeddingsSurvey2019}. The ability to do so depends on the statistical nature of language: certain combinations of words occur more or less frequently together, for example \enquote{green grass} vs. \enquote{green ideas}. In order to capture these observations, distributional models are trained on \emph{corpora}, large datasets of text, based on the assumption that words that occur together, or are used in similar sentences, are more closely related than words that do not. During this process, a set of \emph{stop words}\cite{wilburAutomaticIdentificationStop1992} is often ignored. Stop words are generally taken to be short, common words without any real semantic context. Examples are articles, prepositions and similar words, all of which have signifance in syntax rather than semantics. There are multiple ways\cite{deerwesterIndexingLatentSemantic1990}\cite{sezererSurveyNeuralWord2021} to extract the distributional information to produce the desired word vectors, which can be subdivided into two categories:

\paragraph{Count based models\cite{deerwesterIndexingLatentSemantic1990}} work by counting, for each word in a corpus, how often it occurs near each other word and storing this information in a \emph{cooccurrence matrix}. The definition of near can vary according to the specific strategy used in calculating the word embeddings, but usually means something like \enquote{next to eachother} or \enquote{both occur within a shared 3 word window}. Generally, stop words are removed when building this cooccurrence matrix. The resulting matrix is typically \emph{sparse}\cite{duffSurveySparseMatrix1977}, i.e. containing lots of zeroes. This is undesirable both from a computational point of view, and because this cause for example similarity measures to tend to $0$.

The answer to this issue is to apply some form of \emph{dimensionality reduction}, using linear algebra to effectively find the most relevant axes for a lower dimensional space and embedding the sparse count-based vectors (rows or columns of the cooccurrence matrix) via a change of basis into this new space.

\paragraph{Predict based models\cite{sezererSurveyNeuralWord2021}} begin by assigning each word in the provided corpus to a \emph{one hot encoded} vector or basis vector: A vector with only $0$s and a single $1$ the position of which uniquely identifies the word. Again, stop words have generally been removed from the corpus at this point. Then, training examples are generated based on the chosen model and the cooccurrence of words in the corpus. For example, we consider a \emph{Continuous Bag of Words} (CBOW) model with windows size $1$: each word is only associated with the words directly before and after it. For each word in the corpus, a positive training example is generated as follows: Given a sentence such as \enquote{Jackdaws love my big sphynx of quartz}, the combined input of the vectors associated to the pair (\enquote{jackdaws}, \enquote{my}) should be associated to \enquote{love}\footnote{This example could be read as a fill-in-the-blanks for \enquote{Jackdaws \_ my \dots}, the answer to which should be \enquote{love}.}, the input (\enquote{love}, \enquote{big}) should be associated to \enquote{my}, etc. These training examples are supplemented by negative training examples (signifying word combinations that should not occur together) which can be generated by randomly combining words and removing randomly generated examples corresponding to actually occurring training examples. This training data will then be fed to a single layer neural network. The final resulting weight matrix can then be used to transform a one hot encoded input vector to a word embedding.

The field of distributional semantics and the concept of word embeddings deriving from it form the basis of many forms of textual analysis, which warrants their consideration for this project. Distributional semantics capture one aspect of natural language: statistics. There is, however, another aspect to language of similar importance, which is compositionality\cite{bradleyInterfaceAlgebraStatistics2020}.

%Given the above exploration, the usage of distributional compositional semantics is preferable as a basis for this research project. The applicability, however, largely depends on the availability of higher-order tensors, corresponding to words with special roles such as verbs or adjectives\cite{kartsaklisUnifiedSentenceSpace}, for the Dutch language. As most work on this subject is relatively new, it cannot be guaranteed that high quality higher order word embeddings are readily available for a relatively small language. As a fallback, regular distributional semantics appears to be preferable, due to the requirement for expansive set theoretic models in compositional semantics. Even without tensor availability, some of the lessons provided by distributed compositional semantics may be used to inform this research.

\subsection{Discourse Pragmatics}\label{ssec:prag}
The mentioned existing work on semantics provides a foundation to determine the sentiment of a tweet. However, it should be taken into account that text rarely exist in a vacuum; additional context is required to make sense of an utterance. In the case of tweets, part of this context is determined by the conversational structure provided by Twitter. Tweets can act as a reply to another tweet, and can themselves be replied to. In other words, they form part of a conversation, the content of which is built from the semantics of the individual tweets, but also their interdependence.

The domains concerned with this contextual aspect to meaning in text are the closely related fields of \emph{pragmatics}\cite{levinsonPragmatics1983} and \emph{discourse analysis}\cite{johnsonDiscourseAnalysis2020}. The focus of the latter appears to be on the linguistic components of the context, and that which can be inferred from the surrounding discourse\cite{al-hindawiPragmaticsDiscourseAnalysis2017}. The former discipline tends to shift this notion of context to focus more on external or physical context and to analysing speaker intention. In this regard, discourse analysis appears to be more applicable to the platform at hand, whereas pragmatics more closely aligns to the stated goals of this project as incitement is one potential form of speaker intention. Most ideas referenced from both of these fields exist in the intersection between the two, or arose in one field but find application in the other. The term \emph{discourse pragmatics}\cite{al-hindawiPragmaticsDiscourseAnalysis2017} is used to describe the hybrid field arising from the collaboration between the two subjects and as such will be preferred as the general term for the combination of these fields in this writing;
in most instances, more specific terms will be used to refer to concepts used within discourse pragmatics, as described in the following subsections.

\newthought{For the purpose of this research}, two main ideas appear to be of interest: \emph{Speech acts} and \emph{conversational implicature}. Both are briely considered in the following sections. We futhermore investigate the \emph{dialogic principle} and \emph{pragma-dialogue} due to its relevance to the conversational aspect of tweets.

\subsection{Speech Acts}
A central concept to the field of discourse pragmatics is the concept of \emph{speech acts}, also referred to as an \emph{illocution}, as posited by Austin\cite{austinHowThingsWords1962} and expanded upon by Searle\cite{searleSpeechActsEssay1969}. The central thesis of this concept is that an utterance can be more than merely a statement of information, but can itself be seen as an act with real-world consequences. As human reality is shaped by the power of words, we allow words to impact that reality just as physical actions would. For example, a head of state has the power to enact law or declare or end wars by words --- spoken or written --- alone. Similarly, a parent naming their child will in essence do so by stating a new fact about the world and thereby creating a world in which a newborn child is named.

\newthought{It should be noted that} the result of any speech act depends on context and speaker. The sentence \enquote{The match has begun!} will have the intention and effect of starting the match when uttered by an umpire, on a pitch where two teams have assembled for a match of cricket. The same sentence, uttered by another person in the exact same situation, or by the same person in a different situation, might have the same intention but will not accomplish the same effect. In the former case, the speech act is considered to have been \emph{felicitous}; in the latter case, the speech act fails to be performed in what is referred to as a \emph{misfire} --- the person making the declaration has no authority to start a match in the given circumstances, and as such nothing happens.

A second way in which a speech act can fail to be felicitous is in the case of \emph{abuse}. An example would be when Alice promises Bob to peform an action without intention to follow up on it. In this scenario, the speech act \emph{can} be considered to have been performed, but the act is not felicitous. The rules which dictate whether a speech act can be considered felicitous are called \emph{felicity conditions}.

\newthought{An utterance} cannot be seen separately from intention. For example, the intention behind a question such as \enquote{Do you think it's cold in here?} will in many contexts be to get a person to close a window or turn up the heat, not to start a debate on that person's perception on the temperature. Austin\cite{austinHowThingsWords1962} describes three levels on which a speech act can be analysed:

\begin{itemize}
    \item The \emph{locutionary act} is the actual act of speaking or writing the sentence.
    \item The \emph{illocutionary act} signifies the implied request or demand and represents the intention or purpose of the speech act: What is the speaker trying to accomplish by making a statement?
    \item The \emph{perlocutionary act} is the actual effect of the speech act.
\end{itemize}

In the example of \enquote{Do you think it's cold in here?}, the locutionary act consists of a speaker uttering the question, the illocutionary act is to request the addressee closes the window, and the perlocutionary act is at the very least conveying the speaker's discomfort with the temperature, and potentially persuading the addressee to help solve source of the problem.

Searle\cite{searleSpeechActsEssay1969} goes on to categorise illocutionary acts into five categories:
\begin{itemize}
  \item Assertive or representative, which state a fact one believes to be true, committing to the validity of the preposition, and attempting to convince the receiver thereof;
  \item directives, where one wishes to persuade the receiver to do something, including but not limited to ordering, requesting or suggesting;
  \item commissives, where one makes a promise or threat, or enters a verbal contract;
  \item expressives, which reflect emotions or attitudes such as apologies and expressions of gratitude or (dis)approval;
  \item declarations, which by their utterance (attempt to) change the world by representing its new state, such as christening a child or declaring war.
\end{itemize}

It should be noted that utterances can fall in an overlap between some of these categories: The sentence \enquote{I promise to obey} both commits the speaker to obediance, and declares the promise. The difference between these types of speech acts is relevant to interpreting the intent of the speaker. In the context of incitement, assertives and expressives can for example serve to convince the receiver of a perceived threat or injustice, thereby providing a context for commissives (in this case, threats), directives (suggesting violence) and declarations (of a state war\footnote{In this case, war is used in its broader definition; it is meant to include wars on peoples, groups or concepts within a nation state, not just war between separate political entities.}). This flow could play a part in determining the intended effect of a series of statements.

\subsection{Conversational Implicature}
In order to gauge speaker intention, which has been established can differ from the literal meaning of an utterance it is important to separate what is said from what is implied. The latter is called the \emph{conversational implicature}, and can be detected by how a speaker deliberately fails to obey certain unwritten rules of conversation.

These unwritten rules or \emph{maxims} have been postulated by Grice\cite{griceLogicConversation1975} in the theory of the \emph{cooperative principle}. In it, the assumption is that both speaker and listener are trying to communicate effectively. The listeners should be confident that in any case of ambiguity, the most likely intended meaning is the correct one. In order to achieve this, the speaker will generally obey 4 maxims:

\paragraph{The maxim of quantity} states that the speaker should communicate the right amount of information; they should not leave relevant information out or include unnecessary details. For example, when discussing what main course to order in a restaurant, one could list the available options on a menu. By excluding a dish without good reason\footnote{An example of a good reason to exclude a dish would be to conform to a listeners dietary preferences, thereby favouring the maxim of relation above the maxim of quantity.} or including a dessert, one breaks this maxim.

\paragraph{The maxim of quality} states that the speaker should not communicate information believed to be false, or for which there is insufficient evidence. In the restaurant example, one could break this maxim by suggesting dishes not on the menu. Both deliberate lies and overstatement of confidence in a fact are included in counterexamples to this maxim.

\paragraph{The maxim of relation} states that the speaker should only communicate relevant information to the context at hand. Continuing the example of the restaurant, starting a discussion on the weather or the state of politics whilst deciding what to eat would in general violate this maxim.

\paragraph{The maxim of manner} states that the speaker should communicate in a clear manner, avoiding obscure or ambiguous terms, being succint and not leaving out crucial steps in reasoning. In most cases, listing the original Chinese names of dishes to an English speaker, or adding irrelevant information about the origins of each dish, one could be in violation of this maxim.

\newthought{Grice states that} in general conversation people implicitly and unconsciously try to obey these rules. Any overt deviance, then, could be interpreted as a deliberate \emph{flouting} of a maxim, which in turn signals to the receiver that the information conveyed is not or not merely the information semantically contained within the sentence.

For example, a tweet containing a turn of phrase such as \enquote{It would be a shame if someone were to X} can be understood as, depending on context, being either an honest expression of desire \emph{not to see X happen}, or an covert suggestion to a target audience to perform X. If the concept of X has not been hitherto mentioned and can generally be perceived to be a negative thing, this utterance would at the same time flout the maxim of quantity by introduce more information than needed --- as the negativity of X is common knowledge, flout the maxim of relation --- as prior to this noone was openly considering X to happen, and flout the maxim of manner --- by being more verbose and indirect than appropriate. Incidentally, the remaining maxim of quality is also flouted by the author, who themselves do not accept the generally agreed upon truth of X being negative, which further signals to an informed audience the intended meaning. In this example, the tweet can reasonably be flagged to potentially inciting.

\todo{Huib: is dit waar je op doelde?}
One challenge in the application of Grice's maxims in this research is that it is not immediately obvious how to automatically determine when a maxim is being flouted, as this process requires a lot of context. Some work\cite{vanheeExploringFinegrainedAnalysis2018} has been done on using machine learning, specifically support vector machines, on the automatic detection of irony on Twitter inspired by the Gricean notion of maxim flouting, but it seems this process is mainly informed by Grice instead of directly based on it.

\newthought{More generally}, the concept of implicature relates to the political idea of \emph{dog whistling}\cite{goodinDogWhistlesDemocratic2005}, where a speaker uses a specific euphemistic phrase to signify one thing to one part of the audience (the in-group), and another thing to the rest (the out-group). For a historical example, the phrase \enquote{state rights} has been used to platform racial segregation in the United States\cite{warrenIfItTakes2008}. Instead of providing a direct answer to a question regarding the issue of desegregation, a politician campaigning to maintain the status quo (thereby campaigning against desegregation) would shift the debate to the issue of state rights. By answering a question about A by starting about B, the politican can flout the maxim of relation.

In the case of online communication such as tweets, this process can be used to signify meaning to a target audience --- those being aware of a certain context --- whilst at the same time being readable by a more general audience without conveying the same message. A modern example can be found in the usage of the okay sign emoji in tweets: in certain alt-right communities the associated hand gesture came to be understood as representing the phrase \enquote{white power}\cite{antidefamationleagueHateDisplayHate}, lending context to the usage of the symbol beyond the more generally understood original meaning of \enquote{It's okay} or \enquote{I'm okay}. Using this context a tweet can signal part of an audience within an in-group a different reading of the same text compared to what an out-group audience would understand. For example, a tweet calling out a succesful person of colour and containing the emoji could be read both as an endorsement by the tweeter, or as a call to action for white nationalists. In this case, the intentions of the author can then be considered harmful, while the tweet itself provides a form of plausible deniability.

\subsection{The Dialogic Principle}
In Pragamatics, Discourse, and Cognition\cite{kecskesPragmaticsDiscourseCognition2013}, Horn and Kecskes identify pragma-dialogue as one of three approaches within the field of pragmatics, based on work by  Weigand\cite{weigandLanguageDialogueRules2009}. This field shifts focus to the dialogic nature of interaction, where two interactants act and react. The \emph{dialogic principle} states that speech acts are not communicatively autonomous, but that the smallest possible subdivision is the sequence of action and reaction.

\newthought{In the case of Twitter discourse}, the general trend in conversation does not exactly resemble dialogue, i.e.\ interaction between two constant parties, but rather a multiway conversation where interactants can join and apparently\footnote{Due to the nature of the platform, it can be observed that a interactant ceases to contribute to the discourse, but not whether they actually continue to listen in.} leave without formality. Nevertheless, the focus of this paradigm on action and reaction seems highly relevant to the analysis of incitement and general intent behind tweets. In a general conversation on Twitter, the initial action is a publicly visible tweet or thread\footnote{It is common practice on Twitter to self-react in order to avoid the 280-character limitation on tweet length.} of tweets by a single author which also forms an obvious starting point for any automated system considering a conversation. The subsequent reactions can be split into three broad categories: \begin{inparaenum}\item replies and quoted tweets, i.e.\ publicly interacting with the tweet and adding one own thoughts on the matter; \item likes and retweets, i.e.\ publicly affirming having read the tweet and expressing approval and \item having read the tweet and internalised part of the message without visibly interacting\end{inparaenum}. The last form of reaction is clearly the most common --- having read and at least understood the content of a tweet can be seen as a requirement for further interaction --- and therefore most desirable to use as an indicator. Unfortunately, this type of reaction is also the least visible and thereby hard to consider in any automated capacity. Likes and retweets can be viewed as a rudimentary metric of how often a tweet is read and internalised, but cannot be considered very accurate as the ratio between agreement and publicly expressed agreement is not necessarily the same for different tweets as it may depend on factors such as how vocal the public is and how socially acceptable endorsing the view expressed in a tweet is.

For the purpose of this research, the reactions of replying and quoting are of the most immediate interest, as these reactions are themselves actions which can elicit further reaction. This perspective allows us to consider the tree-like structure formed by tweets and their replies and quotations. Intent can then be analysed on two levels: First on the level of a single tweet, and then on (paths within) a conversation tree. It is part of our hypothesis that the way intent within individual tweets evolves over the course of a discussion can yield patterns which can be used to more accurately judge the intent of individual tweets and the conversation as a whole, allowing extrapolation to locate tweets of interest potentially in advance.

\todo{Huib: sectie hierboven is uitgebreid, hypothese valt hier logisch maar goed om deze in BG te noemen?}


\subsection{Sentiment Analysis}
The problem of detecting emotion from text has been explored to a great degree, combining different fields visited earlier in this chapter. The field of \emph{sentiment analysis (SA)}\cite{feldmanTechniquesApplicationsSentiment2013} overlaps with distributional semantics as described above, generally treating a piece of text as a series of word vectors. This semantic information is combined with \emph{part-of-speech (POS) tags} to form the input for machine learning techniques in order to extract information the sentiment expressed in an utterance.

\subsection{Part of Speech Tagging}
Whereas distributional semantics generally disregards syntactic information contained in a text in favour of the semantic content of the individual words, sentiment analysis frequently includes some form of part-of-speech tagging to distinguish homographs\footnote{Homographs are akin to homonyms in that they denote a set of words with the same spelling, whilst dropping the requirement of also sharing the same pronunciation. For example, the word \enquote{lead} can be interpreted as a verb, meaning to guide, or as a noun, meaning the element. Although these words will rarely be confused in spoken text due to a difference in pronunciation all standard dialects of English, the words are spelled the same and thus in isolation indistinguishable in the context of written text.}. A part of speech in this context describes the grammatical role of a word in a sentence: Verb, noun, determinant, etc. The process of POS tagging is generally based on stochastic methods, frequently employing a \emph{Hidden Markov Model (HMM)}\cite{martinezPartofspeechTagging2012}\cite{kupiecRobustPartofspeechTagging1992}. The hidden states in this context are the parts of speech to be determined for every word in a sentence.

For example, consider the sentence \enquote{Consuming lead will lead to poisoning.} Here, all words can be interpreted as at least two parts of speech, and the word \enquote{lead} occurs twice in different roles. In order to figure out which POS should be assigned to each word, a Hidden Markov Model is employed. The words in the sentence correspond to the emissions of the model:

$$\text{consuming} \to \text{lead} \to \text{will} \to \text{lead} \to \text{to} \to \text{poisoning}$$

The token \enquote{consuming} likely corresponds to a \emph{verb}, but might also be an \emph{adjective} or (rarely) even a \emph{noun}. As this is the first word, the probability for it being a verb does not depend on the previous word, but only on the probability that a sentence starts with a verb\footnote{This probability can be trained on a corpus and forms part of the trained model.} multiplied by the probability that any randomly chosen verb would turn out to be \enquote{consuming}\footnote{This probability is similarly part of a trained model}. The probabilities that the word is an adjective or a noun are calculated similarly. For the second word, there are two possible POS tags: verb or noun. This yields six possible taggings for \enquote{Consuming lead}: $\text{verb} \to \text{verb}$, $\text{verb} \to \text{noun}$, $\text{adjective} \to \text{verb}$, etc. The probability for the tag $\text{verb} \to \text{verb}$ is the product of three probabilities:
\begin{itemize}
  \item The probability that \enquote{Consuming} is a verb, as calculated before;
  \item the probability that a verb follows a verb, without considering the specific verbs and
  \item the probability that a randomly chosen verb will turn out to be \enquote{lead}.
\end{itemize}

By repeating this process, the probabilities for each possible tagging of a sentence can be computed, after which the most probable tagging is chosen.


\section{Tagging Tweets and Conversation Trees}
We tag each tweet with an initial intent/sentiment vector based solely on the tweet itself. Afterwards, we can estimate tags for the reply relations going back up. We view a conversation as a tree-shaped graph. Vertices corresepond to tweets, edges to reply relations.

Combining intent values on tweets is done in three steps. First, the tweets are analysed in isolation, so without consideration of the tweets it replies to, or the tweets that follow. This gives an initial label to each vertex within a conversational tree. After this, we will traverse each path in the tree from top to bottom, which we call the forward search step. Here, the relation between intent of a parent tweet and its child tweet are analysed. This step yields labels for the edges of the tree. Finally, we combine the results bottom to top --- the backward consolidation step. Here, the intent given to child-nodes and the $\delta$-intent given to vertices combine to form an updated valuation for each non-leaf node in a conversation tree. The reasoning behind this step is that if a tweet, which in itself cannot be classified as inciteful, nonetheless solicits replies that are largely recognised as inciteful, the parent tweet should be flagged as interesting as new inciteful replies are more likely to appear in the future.

\subsection{NLP / SA on Dutch Tweets}
State of the art seems to be mostly centred around the BERT model. For the initial sentiment training, we require a model that
\begin{itemize}
  \item Understands Dutch
  \item Outputs sentiment/intent vectors
\end{itemize}
which does not appear to exist. Transfer learning allows us to use pre-trained models and fine-tune for specific languages and use cases. I'm still figuring that out.

It appears Bert accepts labels as potential output, cannot find anything on using vector embeddings. Labels can be translated to 1-hot encoded vectors in a sentiment/intent space which would allow further processing. --- Further reading: this is due to softmax, let's see if we can disable that!

\todo{Need to find tagged data}

Note on reuse of existing models in other languages\cite{devriesGoodNewHow2021}.

\subsection{Intent Embeddings}
In order to tag data for training BERT, we need a standard for how an intent vector space looks. The suggested format is to use $\mathbb{R}^{12}$, specifically $[0,1]^{12}$ for manual tagging of data. The resulting training data vectors will be normalised per participant and then averaged between participants before being used for training BERT. The structure of the intent space for this tagging is hand-crafted, as the initial data is entered by hand instead of learned automatically from context. Table~\ref{tab:intemb} details the meaning assigned to each basis vector.
This might be supplemented by other tweet statistics such as number of likes/retweets and/or author statistics such as number of followers. Main problem with this is that these values are subject to change and add additional requirements on keeping the local data storage up to date. Initial trial will leave these out but this might be added in the future. It might also make more sense to include these statistics in a later stage, when viewing tweets in context instead of in isolation.
\begin{table}[h]
\begin{tabular}{l l p{54mm}}
  $e_{0}$ & \bf Assertiveness & As described by Searle\cite{searleSpeechActsEssay1969}\\
  $e_{1}$ & \bf Directiveness & As described by Searle\cite{searleSpeechActsEssay1969}\\
  $e_{2}$ & \bf Commissiveness & As described by Searle\cite{searleSpeechActsEssay1969}\\
  $e_{3}$ & \bf Expressiveness & As described by Searle\cite{searleSpeechActsEssay1969}\\
  $e_{4}$ & \bf Declarativeness & As described by Searle\cite{searleSpeechActsEssay1969}\\
  $e_{5}$ & \bf Naive Sentiment & Positive wording as in regular SA\\
  $e_{6}$ & \bf Sadness & Tweet expresses sadness / is a call for emotional support \\
  $e_{7}$ & \bf Hostility towards Target & (person, group, situation)\\
  $e_{8}$ & \bf Hostility towards Reader & (general public reading the tweet) \\
  $e_{9}$ & \bf  Call for Action & Expresses specific incitement towards (violent) action \\
  $e_{10}$ & \bf Use of coded language & Use of language indecipherable to out-group \\
  $e_{11}$ & \bf Use of euphemism & Known dog whistles or metaphor \\
  $e_{12}$ & \bf Use of sarcasm & \\
  $e_{13}$ & \bf Question & \\
  $e_{14}$ & \bf Contradiction & Tweet appears to directly contradict another tweet or news item \\
\end{tabular}
\caption{Proposed basis vectors for intent embedding space}\label{tab:intemb}
\end{table}

\section{Tweet Dataset Acquisition}
Single search query in period of one hour. Coronapas?
Then follow each thread up in case of replies.
Finally, download entire tree for each tweet.
