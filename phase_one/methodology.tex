\chapter{Research Methodology}\label{chap:methodology}

This chapter describes and motivates the methodology used in this research project, its objectives, and the central research questions.

%\section{Research Methodology}
\newthought{The primary methodological framework} used for this research project follows the principles of Design Science Research, the application of which to information science was posited by Hevner\cite{hevnerDesignScienceInformation} in  2004. In this approach, the central process consists of of a practical problem, which is then to be progressively solved by the creation of innovative artefacts. This design phase is informed by interpretation on the desires as formulated by the stakeholders and study of existing work in relevant fields. At the same time, the results of this design phase are continuously evaluated, providing further direction for the cycle to repeat with the refinement of existing or creation of new artefacts\cite{johannessonIntroductionDesignScience2014}. This method matches the objective of this project, where the desired outcome is the creation, study and adoption of a novel solution, rather than the study of an already present phenomenon or framework. The focus, then, is twofold: On one hand, study of existing methods and previous research is an integral part of this research. On the other hand, no batteries-included solution is readily available, so innovative exloration and recombination of existing techniques will also be necessary to solve the issues at hand. Using these two approaches, it is the intention of this project to explore the design and evaluation of a possible conceptual model to tackle the issue. The focus herein is on research, to evaluate whether and why the chosen model is applicable, rather than producing a finished, ready-to-market product.

For the scope of this project, the problem to be addressed is that of identifying sentiment, specifically incitement, from short public social media posts such as tweets.
The problem arises from the fact that existing methods of sentiment analysis depend on a certain minimum amount of content in order to correctly predict the general sentiment of a text. Most methods are based on a bag-of-words model, wherein stop words\footnote{generally short common words with little or no semantic content, instead providing syntactic information on how other words relate.} are removed leaving even less text to work with.
A tweet, by definition, is short, and thus on itself may fail to provide adequate textual information for the detection of sentiment. This is further complicated by the fact that tweets are generally comprised of informal language, and can include a large amount of information not recognised as text by naive natural language processing: hashtags, accidental or delibarate misspellings, links, emoji, etc. This leaves the amount of parseable text generally even lower than the 280 character limit imposed by Twitter, and causes every rejected word to have a relatively large impact.

To apply the design science approach to this project, the three cycles of Hevner\cite{hevnerThreeCycleView2007} are used as a guideline. The central design phase cycles between the construction of artefacts to test out theories, and using the results to refine the assumptions for the next cycle.

\section{Subquestions}
\noindent This research follows three separate but codependent cycles as described by Hevner: \begin{inparaenum} \item Collecting domain knowledge [rigour cycle], \item Model development [design cycle], and \item Application context [relevance cycle] \end{inparaenum} The rest of this section will discuss each cycle in more detail and formulate the relevant subquestions. Following this, a summary is provided including a graphical representation of the questions and their dependencies in Figure~\ref{fig:idg}.


% Hernoemen naar RQ-Relevance-1, RQ-Design-1 etc
\subsection{Collecting domain knowledge (rigour cycle)}
The first cycle is designed to collect and evaluate domain-specific knowledge and existing solutions. This phase will be guided by the following questions:\\[5mm]

  %\item Ri1: \enquote{What can be learned from the present use of discourse pragmatics and sentiment analysis to extract incitement from text?}
  \newthought{ \textsf{Ri1}: \enquote{What indicators from discourse pragmatics and sentiment analysis approaches have the highest correlation with the level of incitement?} }\\[2mm]

  \noindent This question is intended to get a grounded overview of applicable indicators in the domain of discourse pragmatics pertaining to sentiment, and in the domain of sentiment analysis pertaining to conversations, in order to detect emotion --- specifically incitement. This question will be used to fuel the design process required to answer \textsf{De1} and will be part of the literature review for this project. Relevant papers will include one or more of the following:
    \begin{itemize}
    \item The application of sentiment analysis as regarding to the detection of incitement or negative emotional content.
    \item The application of discourse pragmatics as regarding the evolution of emotional content within conversations.
    \end{itemize}

        % Doel is indicatoren uit conversation analysis mbt sentiment en uit sentiment analysis mbt conversations om hieruit emotie en specifiek incitement te halen
        %
        % Hoe? literatuurstudy, met de volgende criteria (stuk beneden)
        % Doel: puzzelstukje, relatie volgende vraag: wat doe ik met puzzelstukje, waarom nodig?

  %\item Ri2: \enquote{What approaches have been used to extract information from the structure of conversation in other domains that can be adapted to the extraction of sentiment?}
  \newthought{ \textsf{Ri2}: \enquote{What are the most prominent approaches that have been used to extract information from the structure of conversation in other domains that can be adapted to the extraction of sentiment?} }\\[2mm]

  \noindent This question connects existing NLP techniques to the handling of structured text, as used for distinct yet comparable applications, to the task at hand. It will be used to fuel the design process required to answer \textsf{De2} and will be part of the literature review for this project. Relevant papers will include one or more of the following:
    \begin{itemize}
    \item Distributional NLP approaches used to analyse the semantic content of natural language text.
    \item Compositional NLP approaches used to analyse the syntactic structure of natural language text.
    \item NLP approaches used for structured conversations consisting of trees of short messages.
    \end{itemize}

  %\item Ri3: \enquote{How can the performance of incitement-detection Artificial Intelligence be benchmarked?}
  \newthought{ \textsf{Ri3}: \enquote{What is an adequate and feasible benchmark for the performance of incitement-detection Artificial Intelligences?} }\\[2mm]

  \noindent This question explores how a potential solution could be compared to existing or future solutions, which anticipates and facilitates the evaluation step of the design cycle and thus connects to \textsf{De3}. This step will ultimately help in assessing the value of the results of this project and will be the final part of the literature review for this project. Relevant papers will include one or more of the following:
    \begin{itemize}
    \item The existance of benchmarks containing elements of sentiment analysis, specifically incitement detections.
    \item The formalisation of benchmarks regarding different yet similar problems within the domain of NLP and/or discourse pragmatics.
    \end{itemize}

\subsection{Model development (design cycle)}
In this cycle, a prototype AI will be developed to extract sentiment and incitement information from conversational trees of text fragments.\\[5mm]

  \newthought{ \textsf{De1}: \enquote{How can existing insights on incitement-detection from conversational and sentiment analysis be used to steer the developtment of a semantic, sytactic and structural model of analysis?} }\\[2mm]
    \noindent This question serves to connect the grouding developed in the answering of \textsf{Ri1} to the design of the artefacts utilised to test these ideas in the context of this project.

  \newthought{ \textsf{De2}: \enquote{How can existing models of semantic, syntactic and structural analysis on conversations be adapted to detect incitement?} }\\[2mm]
    \noindent This question is similarly connected to \textsf{Ri2}, aiming to apply the models available within the NLP domain to the design cycle of this research.

  \newthought{ \textsf{De3}: \enquote{How do the results of the design cycle evaluate using the developed benchmarks?} }\\[2mm]
    \noindent This question formalises the evaluation step of the design cycle, applying predefined metrics to the produced artifacts and gauging their effectiveness. The acceptance criteria for this research question are dependent on the results of \textsf{Ri3}.

\subsection{Application context (relevance cycle)}
The final cycle is concerned with the applicability of the prototype artifacts to the goals as formulated by the stakeholders of this project. As this project is primarily connected to a larger research projects that is still in the planning stages, verification by testing the prototype in its intended environment is unfeasible considering the timescale dictated by a master's thesis project.
As future adoption of the prototype and/or technologies derived from it are presupposed, the relevance cycle of this project will instead be focussing on guidelines for eventual real-life application.\\[5mm]

%The final cycle is concerned with how to interpret the results of the development cycle using predetermined benchmarks. Additionally, an attempt will be made to interpret the resulting prototype, which is likely to have developed a black-box component due to the application of machine learning and statistical techniques. By evaluating and interpreting the emergent behaviour, discovered patterns can be formalised in order to promote reproducability and traceability.

  The primary potential issues with the adoption of the technologies under scrutiny are concerned with the ethical considerations involved in applying such an AI to user-generated data without prior consent or reasonable user-expectation on the usage of their data, and the implications should the technology be adapted beyond the initial scope of incitement detection and into more thorough invasion of an individuals thoughts. To scrutinise these issues, the following two questions have been formulated:

  \newthought{ \textsf{Re1}: \enquote{Which primary ethical dilemmata should be considered in the design and exploitation of the prototype artefact?} }\\[2mm]

  \newthought{ \textsf{Re2}: \enquote{What ethical recommendations should be considered in the design and exploitation of the prototype artefact?} }\\[2mm]

  \noindent In order to answer these questions, the first step would be to perform an ethical check by interviewing one or more people with relevant ethical knowledge and to get their view on what dilemmata are involved in the exploitation of the proposed prototype (\textsf{Re1}). A priori, the following considerations will be included in these interviews:
\begin{itemize}
  \item Ethical considerations on working with user-generated data without prior consent.
  \item Ethical considerations on automated vetting of private citizens and the prevention of misuse.
\end{itemize}

Following this, the identified dilemmata will be subjected to the following ethical standards (\textsf{Re2}) in order to formulate recommendations:
  \begin{itemize}
    \item Virtue Ethics (Aristotle),
    \item Deontology (Kant),
    \item Utilitarianism (Stuart Mill) and
    \item Justice as Fairness (Rawls)
  \end{itemize}

  Section~\ref{sec:ethics} contains a more thorough discussion on the expected concerns, which will be used as the basis of the interviewing phase described for \textsf{Re1}.

        %\todo{Aanbevelingen}
        % Wat is gegeven de n primaire ethische frameworks de aspecten etc
        % util, virtue, rights, +fairness
        %\todo{Dit is geen literatuur, maar ethical check (HBO-ICT)}

%This question considers the ethical implications of this research project and its desired artefacts. For this part of the literature study, relevant studies must include at least on of the following:
%\begin{itemize}
  %\item Ethical considerations on working with user-generated data without prior %consent.
  %\item Ethical considerations on automated vetting of private citizens and the prevention of misuse.
%\end{itemize}
\subsection{Summary}
In summary, the project has been divided into three relevant sub-problems, each represented in the rigour- and design cycles. These three sub-problems correspond to the two domains of knowledge relevant for the case at hand, supplemented with the need for benchmarking the results. The relevance cycle is less substantive for this project, as the results will be unlikely to be adopted in time for the duration of this research project. Consequently, this cycle will be limited to a consideration of the ethical implications of adopting an AI system as considered in this study. The internal dependencies of the total project are visualised in Figure~\ref{fig:idg}.
\begin{figure}[h]
\centering
\begin{tikzpicture}
     \node[] (ri1)         {\sf Ri1};
     \node[below=of ri1] (ri2)         {\sf Ri2};
     \node[below=of ri2] (ri3) {\sf Ri3};
     \node[right=2 cm of ri1] (de1)         {\sf De1};
     \node[below=of de1] (de2)         {\sf De2};
     \node[below=of de2] (de3) {\sf De3};
     \node[right=2 cm of de1] (re1)         {\sf Re1};
     \node[right=2 cm of de2] (re2)         {\sf Re2};
     \draw[dotted] (-0.7,-4) rectangle (0.8,1);
     \draw[dotted] (2.1,-4) rectangle (3.5,1);
     \draw[dotted] (4.9,-4) rectangle (6.3,1);
     \draw (-1.2,1.8) rectangle (1.2,2.3);
     \draw (1.6,1.8) rectangle (4.0,2.3);
     \draw (4.2,1.8) rectangle (7.0,2.3);
     \node[above=15mm of ri1] (rigour)         {\bf Rigour Cycle};
     \node[above=15mm of de1] (design)         {\bf Design Cycle};
     \node[above=15mm of re1] (relevance)         {\bf Relevance Cycle};
     \draw[-latex, dotted] (ri1) edge[out=25,  in=155] (de1);
     \draw[-latex, dotted] (de1) edge[out=205, in=335] (ri1);
     \draw[-latex, dotted] (ri2) edge[out=25,  in=155] (de2);
     \draw[-latex, dotted] (de2) edge[out=205, in=335] (ri2);
     \draw[-latex, dotted] (ri3) edge[out=25,  in=155] (de3);
     \draw[-latex, dotted] (de3) edge[out=205, in=335] (ri3);
     \draw[-latex, dotted] (re1) edge (re2);
     \node (a) at (2.9,2.4)       {};
     \node (b) at (5.5,2.4)       {};
     \node (c) at (5.5,1.7)       {};
     \node (d) at (2.9,1.7)       {};
     \node (e) at (0.1,2.4)       {};
     \node (f) at (2.7,2.4)       {};
     \node (g) at (2.7,1.7)       {};
     \node (h) at (0.1,1.7)       {};
     \draw[-latex, line width=1.0mm] (a) edge[out=25, in=155] (b);
     \draw[-latex, line width=1.0mm] (c) edge[out=205, in=335] (d);
     \draw[-latex, line width=1.0mm] (e) edge[out=25, in=155] (f);
     \draw[-latex, line width=1.0mm] (g) edge[out=205, in=335] (h);
     \draw[dotted] (rigour) -- +(0,-1cm);
     \draw[dotted] (design) -- +(0,-1cm);
     \draw[dotted] (relevance) -- +(0,-1cm);
  \end{tikzpicture}
\caption{Internal Dependency Graph}\label{fig:idg}
\end{figure}

\section{Sprint Planning}
  In a practical sense, the work needed for the completion of this project will be organised in sprints. Sprints are organised using vertical slicing, meaning that each sprint works to further all seven research questions, ideally in more or less equal measure. However, the first few sprints are expected to disproportionately favour \textsf{Ri1} and \textsf{De1} in order to compensate for the gap in previous knowledge as compared to the subject matter of \textsf{Ri2} and \textsf{De2} respectively. The latter two questions are more closely alligned to experience accumulated in the courses and projects of this master's track, which prioritises achieving relative parity early in the project.

  As each sprint includes work towards answering seven research questions, the sprint duration chosen should be adequately substantial to ensure progress can be made on all of these within a single sprint. Conversely, the project is of fixed duration, and should contain sufficiently many sprints to facilitate the feedback loop desired in the design science paradigm. Balancing these two desires implies a sprint duration of 2 weeks to be a workable compromise.


\section{Literature Review Methodology}\label{sec:literature}
As this project works towards applying existing Natural Language Processing (NLP) technology to a novel problem domain, that of sentiment analysis and discourse pragmatics, it is of vital importance that the literature review part of this endeavour is both expansive as well as grounded in rigour. As the problem domain and the domain of the intended solution are largely disjunct, it is important to include both in this researsch phase, as well as any intersection previously touched upon by research.

In order to ensure a thourough and complete literature review phase, we adopt an approach where we first establish selection criteria on which information would be relevant and need to be included. This would then be followed by a literature search and one or more iterations of refinement. The goal of this cycle is to get a grip on the best working hypotheses on how to detect incitement before proceeding to test the results on the data at hand. The sources included as the starting point of the literature search are the following:

\begin{itemize}
  \item Web of Science,
  \item ACM Digital Library,
  \item Science Direct,
  \item Springer Link,
  \item Wiley Online,
  \item WorldCat.org and
  \item SAGE
\end{itemize}

In order to facilitate searching these sources, an aggregate search engine provided by the Hogeschool Utrecht is used. The search terms are based on the criteria sketched for each research question above, and are refined to produce a reasonable number of results per question. If the number of articles is prohibitively high even after refinement, a select and truncate approach is used based primarily on the number of citations each article has. This ensures that established and relevant literature is considered in this process. After an initial selection has been made, a second round of selection is based on the relevance determined by reading the abstract, introduction and conclusion of the articles.

%\subsection{Selection Criteria}
%The literature study for this research project corresponds primarily to the rigour  cycle, which in turn consist of three research questions. Which literature is to be included for each of these questions has been sketched out above.

%\todo{sorteren / truncate / keywords aanpassen <- boekje}


%\section{Research Strategy}

% - Software prototype
% - Conceptual Models

% - Design as an artefact
% - Problem relevance
% - Design evaluation
% - Research contributions
% - Research rigor
% - Design as a search process
% - Communication of research

% Challenges
% The research  approach  adopted  in  this  thesis  includes  the  design  science  methodology  as  an overall research method
%


