\chapter{Risks, mitigation and ethics}\label{chap:risks}
This chapter addresses the risks involved in this research project, and discusses potential ways these problems can be avoided if possible and mitigated if necessary. Additionally, it includes a short sketch of some ethical consideration which form the basis of the material that will be addressed in \textsf{Re1}.

\section{Risks}\label{sec:risks}
As is always the case with any serious project, successful completion is never guaranteed. This section details the risks identified for this project, and the measures planned to mitigate the impact of these risks on the completion of this project.

\subsection{Quantity and Quality of Available Relevant Literature}
The literature review is a significant part of this project, forming the basis for the design cycle and prototype artefact. As such, a large part of this research is directly or indirectly dependend on the succesful acquisition of relevant literature. This potential problem is most serious in the context of \textsf{Ri1}, as this question both addresses a novel domain\footnote{For NLP, previous experience has been provided by the masters course.} and the literature study is projected to be the primary means of answering this question\footnote{Compared to \textsf{Ri3}, where interviewing relevant stakeholders forms a second part of the methodology.}.

\paragraph{Mitigation}\mbox{}\\[2]\noindent
In order to mitigate this risk, should this problem arise, an alternative method of acquiring information is needed. For the specific case of \textsf{Ri1}, we anticipate that the HU Research Group would be able to provide expertise on the subject. As the experiences here are estimated to be relevant, but not entirely specific to the case at hand, this route would provide an adequate, if less satisfactory, means to answer the research question.

This risk is overall not very likely with a probability estitmated at 3/10; the impact to the quality of the end result is small due to adequate possibility of mitigation, estimated at 2/10.

\subsection{Applicability of Literature Study Results on Artefact}
The purpose of the literature review is to inspire the design cycle of this project, and as such must not only yield results, but also provide enough basis to be translatable to a prototype solution. This is of course, not guaranteed.

\paragraph{Mitigation}\mbox{}\\[2]\noindent
If this scenario were to occur, the most prudent course of action would be to enlist help from colleagues via short sparring sessions. It should be noted that this risk also plays into a personal tendency to overthink, which must be avoided by committing to move forward even when stuck. As this part of the process is the most reliant on creativity, it is better to follow a path that turns out to be, in retrospect, suboptimal than to grind to a halt: the former can, at the very least, provide insights into why an approach was fruitless and possibly inspiration on how to move forward. I will depend on my daily supervisor to remind me of this, if necessary.

This risk is overall somewhat more likely but still manageable with a probability estitmated at 4/10; the impact to the quality of the end result is small due to adequate possibility of mitigation, which is also estimated at 2/10.

\subsection{Absence of Desired Structural Features in the Twitter API}
The desired approach to solving the main problem inspiring this research project depends on analysis of the structure of Twitter conversations. It is projected that this information can sufficiently be extracted from Twitter using the APIs. For this, access to the Twitter API is assumed --- this could be considered a risk which has already been mitigated. Should there be any problem in extracting the desired information, this will produce delays in the realisation of the prototype artefact.

\paragraph{Mitigation}\mbox{}\\[2]\noindent
The situation described above allows for a number of different solutions, some of which can be set in motion concurrently and the choice of which impacts the expected quality of the result of this research: An attempt can be made to manually reconstruct the tree-like structure from tweets, but this approach is quite labour-intensive. An alternative would be to retrieve the structure from Twitter via web-scraping. This will also cost some time, but can more easily be outsourced to an HBO-student as it is mainly a technical challenge that could be relevant enough to warrant a project for undergraduates. The main risk then would be to start and finish such a project on relatively short notice. A third approach, using mock-data, can be viewed as the ultimate fallback: Though it is guaranteed to be possible and the time-investment necessary is relative to the amount of data required\footnote{Inversely, the ammount of time available can thusly dictate the amount of mock-data that can be feasibly created.}, the results are likely to be of significantly lower quality than the other two approaches using actual Twitter data. Mock-data would allow the testing of the prototypes performance in a quantative sense, but any qualitative results would be largely dependent on the mock-data and therefore would not give an accurate reflection of real performance.

Given the three potential solutions mentioned above, it would be prudent to explore different avenues simultaneously: if the second route can indeed be outsourced, this would create the potential to generate high-quality data (indistinguishable from the data that would be extracted from the API) whilst time could concurrently be invested in manual labelling or generation of adequate mock-data. The latter choice would then depend on the time available.

This risk is probably the most likely with a probability estitmated at 5/10; the impact to the quality of the end result is also larger, and estimated at 6/10.

% Na Res Meth / hoofdstuk 4 /  "planning, risico's en mitigaties":
% Projectmatige info
% Planning: wanneer is wat af, 25% versie etc, tests - grove milestones
% - hoeveel tijd gaat het kosten voor bepaalde elementen

% Risico's noemen, aangeven wat er mis kan gaan en hoe hiermee om te gaan - Risk / mitigation
%
% 4: planning risico's mitigations
% 4.1 planning
% 4.2 risk/mitigation
%
% RISKS
% - voldoende literatuur vinden? (relevant)
% - resultaten van literatuur vertaalbaar naar bruikbare code
% - blijkt lastiger tweets te linken

\section{Ethical Considerations}\label{sec:ethics}
As with any application of Artifical Intelligence, Machine Learning and related technology, it is paramount to consider the ethical implications of the project and its results in advance. In the case of this project, the prime ethical concerns can be subdivided into two broad classes: those regarding the methods used to gather and interpret data, and the ethical implications of the finished product.

\subsection{Implications of data usage}
In this project, public discourse will be utilised to extract sentiment information. Specifically, the prototype to be developed and evaluated will be targeted at tweets, i.e. short\footnote{140 characters prior to 2017, 280 afterwards} text messages, potentially including images, links, emoji and hashtags. The tweets considered in the development of this prototype are all public: the original author has at minimum agreed to the public visibility and traceability of their input, or has actively intened this.

On the other hand, the author might not have expected their input to be subjected to additional scrutiny, be it by human eye or artificial intelligence, and may not agree to their information being used in this manner. This is exacerbated by the fact that the original dataset supplied for this research consists of a series of tweets regarding a specific incident covered in Dutch media in May 2013, a period of time during which public awareness of online privacy was more limited than it is now.

In order to proceed, a few options are on the table:
\begin{itemize}
  \item Using the data to train and evaluate the prototype, publishing the results without consideration of author consent (ignore this concern);
  \item using the data to train and evaluate the prototype, refraining from publication of individual tweets;
  \item using the data to train and evaluate the prototype, and asking for explicit permission before quoting individual tweets in publication;
  \item preemptively notifying authors that their tweets are used and for what purposes, removing tweets from the dataset on explicit objection (opt-out);
  \item preemptively asking consent (opt-in).
\end{itemize}

Of these options, the most favourable options from the researcher's point of view --- balancing ethics and practical considerations --- appears to be the second: using the data to train and evaluate the prototype and nothing else. In this scenario, if tweets are required in publication to outline (parts of) the process, mock data will be used instead. The main arguments for this approach are twofold: Firstly, the amount of data used per author is small, such that tweets by any individual author will be unlikely to produce identifiable artefacts within the final results. Secondly, the process of contacting people about tweets made eight years ago is not only unpractically intensive, but also likely to be more invasive than the data-usage in itself, specifically considering the first argument. Most people will not have any active memory on what they have posted online almost a decade ago, and bringing this to their attention will likely be met with apprehension both regarding the original subject matter and the persistence of data. Considering these arguments, the latter three options appear to suggest a cure worse than the disease, albeit on varying scales, and as such can be rejected. Of the remaing two options, the first option is simply to ignore the stated concern. Even though this concern is considered minor, the second option can be considered more ethical and thus should be selected.

\subsection{Implications of the technology}
The second, more pervasive concern is that of the product this project envisions and aims to expedite. Though at first the technology to allow authorities to better assess popular sentiment, de-escalate problematic conversations and mitigate results of incitement appears virteous, this notion entirely depends on the benevolence of the user. The same technology used to combat potential domestic terrorism could, with little to no alteration, be utilised by a more repressive government to monitor and control its population in increasingly invasive manners. Similarly, the user of such technology might not constitute a legitimate authority altogether, but a privately owned corporation whose intentions could be orthogonal to the rule of law.
Finally, even with good intentions on the part of the user, it has been shown that statistical models trained on data are liable to internalise undesirable societal prejudice, potentially leading to a system prone to cast undue suspicions on vulnerable societal minorities.

Therfore, the consideration on how a finished product could be designed to work in an ethical manner requires further research. As such, this issue has been given its own research question:

%\begin{itemize}
%\end{itemize}

As the question on how to guarantee techological progress is used ethically is, in a way, as old as technology itself, this domain has likely been studied intensively. As such, this question will be incorporated into the literature study as part of the relevance cycle.

%\todo{Huib, same here: heb jij nog goede suggesties?}
%\todo{Vraag gaat naar: hoe kan dit misbruikt worden / hoe ga ik dat tegen}
%\todo{naast 7 key requirements EU leggen}
