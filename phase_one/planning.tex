\chapter{Project Planning}\label{chap:planning}

This chapter provides an overview of the planning of the rest of this project, the major milestones and relevant dates.
In accordance with the chosen methodology, the main work for this project has been subdivided into three cycles, each structured into a set of subquestions. For the rigour cycle, each subquestion is scoped by a definition of done, the fulfillment of which is considered the associated milestone.

For the design cycle, each subquestion is linked both to a rigour question, as well as to the design cycle as a whole. Milestones for the design cycle are informed by the development of the application as a whole, organised using \emph{vertical slicing} as elaborated upon in Section~\ref{sec:vs}. Due the nature of this structuring, milestones for the design cycle will consider the prototype as a whole. The exception to this is \textsf{De3}, where a separate evaluation of the benchmark has been included as a milestone.

For the relevance cycle, two major milestones correspond to the two separate subquestion therein: Firstly, defining the ethical dilemmata associated with this project and the exploitation of its results, and secondly the interpretation of these dilemmata according to the four frameworks presented in the elucidation of \textsf{Re2}.

\section{Vertical Slices}\label{sec:vs}
The design cycle corresponds to the development and testing of the prototype, the process of which will be structured according to the Agile\cite{beck2001agile}
paradigm utilising practices from GTD\cite{gtd} and Scrum\cite{tnnpdg}, the latter insofar as these apply to individual projects. One concept adopted for this project from Agile in general and Scrum in particular is the \emph{vertical slice}\cite{ratnerVerticalSlicingSmaller2011}. Applied to the context of this research, a vertical slice refers to a cross-sectional slice through each of the component subquestions of this project, furthering both the literature review associated with each subquestion within the rigour cycle and implementing the results in the prototype as part of the design cycle. This process repeats in 2-week sprints, most of which will also contain part of the work required for the relevance cycle. Not every sprint has milestones for every subquestion, as the milestones correspond to more significant steps in the execution of the project.

%\todo{follows from previous chapters}

\section{Milestones}
This section lists the proposed milestones in chronological order and provides a definition of done for each. Figure~\ref{fig:deps} illustrates the dependencies between the various milestones.\\[3mm]

\begin{figure}[h]
  \centering
  \includegraphics[width=11cm]{figures/milestone-deps}
  \caption{Dependency Graph}\label{fig:deps}
\end{figure}

\begin{milestone}{Ri1--1}{Survey of Sentiment Analysis}{2021-10-22}
  \item Finalised list of relevant papers on Sentiment Analysis;
  \item summary of found indicators and
  \item summary of applicability of indicators in computer code.
\end{milestone}

\begin{milestone}{Ri1--2}{Survey of Discourse Pragmatics}{2021-10-22}
  \item Finalised list of relevant papers on Discourse Pragmatics;
  \item summary of found indicators and
  \item summary of applicability of indicators in computer code.
\end{milestone}

\begin{milestone}{Ri2--1}{Strategies for Semantic Analysis of Tweets}{2021-10-22}
  \item Quantative overview of feasiblity of using syntactic information from tweets;
  \item initial recommendations on utilisation of available syntactic information to inform semantic analysis on individual tweets;
  \item updated overview of required functionality for \textsf{Ri2--2}.
\end{milestone}

\begin{milestone}{Pres--1}{Start Research Group Presentation}{2021-10-26}
  \item Presentation on research plans and methodology to the AI research group of the host organisation.
\end{milestone}

\begin{milestone}{Ri2--2}{Strategies for Structural Analysis of Conversations}{2021-11-05}
  \item Updated recommendations on utilisation of available syntactic information to inform semantic analysis on individual tweets and
  \item strategy for combining tweet-level analysis results in conversation trees;
\end{milestone}

\begin{milestone}{Ri3--1}{Benchmark Exploration}{2021-11-05}
  \item Overview of existing benchmarks in related problem spaces;
  \item suggestion(s) for applicable benchmark to this problem space and
  \item tagged data to determine applicability of benchmark concepts.
\end{milestone}

\begin{milestone}{Re1--1}{Overview of Ethical Dilemmata}{2021-11-05}
  \item Interview transcription on which ethical dilemmata apply to the development and training of the prototype and
  \item interview transcription on which ethical dilemmata apply to the autonomous exploitation of the prototype and derivative technology on real-world data.
\end{milestone}

\begin{milestone}{De--1}{Strawman Implementation}{2021-11-19}
  \item Prototype version containing at least stub implementations of all required functionality;
  \item evaluation of potential shortcomings of current prototype direction and
  \item informed decision on whether to continue this line of reasoning.
\end{milestone}

\begin{milestone}{Ri3--2}{Finalised Benchmark Strategy}{2021-11-19}
  \item Comparison of benchmarks based on tagged data from \textsf{Ri3--1} and
  \item recommendation on benchmarking strategy to use on prototype and future development based on research.
\end{milestone}

\begin{milestone}{Re2--1}{Draft Ethical Recommendations}{2021-11-19}
  \item Overview of ethical frameworks as mentioned in \textsf{Re2};
  \item draft application of frameworks on dilemmata identified in \textsf{Re1--1}.
\end{milestone}

\begin{milestone}{De3--1}{Evaluation of Benchmark}{2021-12-03}
  \item Implementation of benchmark on prototype output;
  \item documentation of results on prototype;
  \item documentation on design choices made in benchmark definition and
  \item documentation on how to apply benchmark on potential future projects.
\end{milestone}

\begin{milestone}{Re2--2}{Ethical Recommendations}{2021-12-03}
  \item Interview transcription on soundness of draft application as delivered in \textsf{Re2--1}.
  \item structured report on ethical recommendations based on these results.
\end{milestone}

\begin{milestone}{De--2}{Final Prototype}{2021-12-17}
  \item Demonstrable prototype including all required functionality;
  \item benchmarking of prototype according to results of \textsf{De3--1};
  \item documentation on design choices made during prototype development and
  \item recommendations on how to improve / continue research direction or alternatively
  \item overview of lessons learned and recommendations on how to proceed.
\end{milestone}

\begin{milestone}{The--1}{Thesis Draft}{2021-12-17}
  \item Draft version of the final thesis for supervisor review.
\end{milestone}

\begin{milestone}{The--2}{Thesis}{2022-01-14}
  \item Final version of the thesis for presentation to the Utrecht University.
\end{milestone}

\begin{milestone}{Pres--2}{Final Research Group Presentation}{2022-01-21}
  \item Presentation on research execution and results to the AI research group of the host organisation.
\end{milestone}

\begin{milestone}{Pres--3}{Thesis Defence}{2022-01-28}
  \item Final presentation of research project and results to university supervision.
\end{milestone}



% \begin{table}
% %\begin{fullwidth}
  % %\rotatebox{90}{
  % \resizebox{11cm}{!}{
  % \begin{tabular}{p{8cm}|l}
    % \bf Milestone & \bf Projected End \\
    % \hline
    % \textbf{\textsf{Ri1-1}: Overview} \begin{itemize}\item a\end{itemize} & % 2021-10-03 \\
    % %\textsf{Ri1-2}: Overview & foo & 2021-10-03 \\
    % %\textsf{Ri2-1}: Overview & foo & 2021-10-03 \\
    % %\textsf{Ri2-2}: Overview & foo & 2021-10-03 \\
    % %\textsf{Ri3-1}: Overview & foo & 2021-10-03 \\
    % %\textsf{Ri3-2}: Overview & foo & 2021-10-03 \\
    % %\textsf{De-1}: Strawman & foo & 2021-10-03 \\
    % %\textsf{De-2}: Final Prototype & foo & 2021-10-03 \\
    % %\textsf{Re1-1}: Dilemmata & foo & 2021-10-03 \\
    % %\textsf{Re2-1}: Draft Ethical Recommendations & foo & 2021-10-03 \\
    % %\textsf{Re2-2}: Ethical Recommendations & foo & 2021-10-03 \\
    % %\textsf{Pres-1}: Start Research Group Presentation & foo & 2021-10-03 \\
    % %\textsf{Pres-2}: Final Research Group Presentation & foo & 2021-10-03 \\
    % %\textsf{The-1}: Thesis Draft & foo & 2021-10-03 \\
    % %\textsf{The-2}: Thesis & foo & 2021-10-03 \\
    % %\textsf{Pres-3}: Thesis Defence & foo & 2021-10-03 \\
  % \end{tabular}
% }
% \caption{Milestones}\label{tab:milestones}
% %\end{fullwidth}
% \end{table}


%- overview of indicators (Ri1) -- wanneer "klaar"? wanneer stop ik?
%- ... dit per rigour deelvraag
%- tussenversies prototypes - strawman prototype (beslispunt werkt dit, halverwege) (De12)
%- benchmark (De3) - beslismoment - doet dit wat het moet doen?
%- dilemma's (klaar halverwege)
%- suggesties (klaar laatste sprint voor eind)
%- eindproduct
%- scriptie
%- presentatie

%--> samenhang
