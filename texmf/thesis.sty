\ProvidesPackage{thesis}

\usepackage{CJKutf8}
\usepackage{csquotes}
\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing}
\usepackage{soul}
%\usepackage[inline]{enumitem}
\usepackage{paralist}
\usepackage{graphicx} % allow embedded images
  \setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
  \graphicspath{{graphics/}} % set of paths to search for images
\usepackage{amsmath,amssymb,amsthm,amsfonts}  % extended mathematics
\usepackage[normalem]{ulem}
\usepackage{calligra}
\usepackage{booktabs} % book-quality tables
\usepackage{units}    % non-stacked fractions and better unit spacing
\usepackage{multicol} % multiple column layout facilities
\usepackage{lipsum,cancel}   % filler text
\usepackage{fancyvrb,xcolor} % extended verbatim environments
  \fvset{fontsize=\normalsize}% default font size for fancy-verbatim environments
\usepackage{tikz-cd,bbm,mathbbol}
\DeclareSymbolFontAlphabet{\mathbbl}{bbold} %let's you use \mathbbl{k} for a field k
\setcounter{secnumdepth}{2}
\usepackage{enumerate}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{nccmath}
\usepackage{qtree}
\usepackage{etoolbox}
\usepackage{hyperref}
\usepackage{quoting,xparse}
\usepackage{framed}
\hypersetup{
    pdfborder = {0 0 0},
    colorlinks = true,
    linkcolor = Purple,
    citecolor = RubineRed,
    urlcolor = RubineRed, % Cerulean,
    linktocpage
}


\theoremstyle{definition}
\newtheorem*{definition}{Definition}

\usepackage{tai}

\providetoggle{clean}

\newcommand{\draft}{\togglefalse{clean}}
\newcommand{\final}{\toggletrue{clean}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Own stuff                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setdefaultenum{(i)}{}{}{}

% Filter only alphabetic characters (for turning arbitrary text into labels)
\def\@az@#1{\ifx\relax#1\else\ifcat a\noexpand#1#1\else\fi\expandafter\@az@\fi}
\def\@az#1{\@az@ #1\relax}

% Create an empty list of todos so that we can append any \todo{} we encounter to it
\let\todos\@empty

% Print argument in red text and add to global list of todos; include reference to pagenumber
\newcommand{\todo}[1]
{
  \nottoggle{clean}
  {\noindent\textcolor{red}{ToDo: #1}\label{\@az{#1}}}{}
\g@addto@macro\todos{\item {[\pageref{\@az{#1}}]} #1}
}

% Typeset a research question and define macros (using roman numerals) to refer to questions by number/reference, full text and text+number.
\def\RQ#1#2{
 \item \emph{#2} (RQ #1)\label{#2}
 \global\expandafter\def\csname RQ#1\endcsname{RQ \hyperref[#2]{#1}}
 \global\expandafter\def\csname RQ#1text\endcsname{\emph{#2}}
 \global\expandafter\def\csname RQ#1full\endcsname{RQ #1: \emph{#2}}
}

% Math aliases
\def\ssstyle{\mathsf}
\def\lan#1{\ensuremath{\mathsf{L_{#1}}}} % Numbered/labeled language variable
%\def\lit#1{\ensuremath{\lfloor\mathsf{#1}\rfloor}} % Literal translation chain
%\def\idiom#1{\ensuremath{\lceil\mathsf{#1}\rceil}} % Idiomatic translation chain
%\def\ctrl#1{\ensuremath{\lfloor\mathsf{#1}\rceil}} % Literal->Idiomatic translation chain
%\def\can#1{\ensuremath{|\mathsf{#1}|}} % Canonical translation
\def\lit#1{\ensuremath{[\mathsf{#1}]^\ssstyle l}} % Literal translation chain
\def\idiom#1{\ensuremath{[\mathsf{#1}]^\ssstyle i}} % Idiomatic translation chain
\def\ctrl#1{\ensuremath{^\ssstyle l\![\mathsf{#1}]^\ssstyle i}} % Literal->Idiomatic translation chain
\def\can#1{\ensuremath{[\mathsf{#1}]^\ssstyle c}} % Canonical translation
\def\tran#1{\ensuremath{[\mathsf{#1}]}} % Abstract translation (chain)
\def\d#1#2#3{\ensuremath{d(#1{\lan{#2}}, #1{\lan{#3}})}} % Distance between two translation chains of a given type
\def\s#1#2#3{\ensuremath{\sigma(#1{\lan{#2}}, #1{\lan{#3}})}} % Distance between two translation chains of a given type

% Use calligra for script/calligraphy in math, as default font doesn't support this
\DeclareMathAlphabet{\mathscr}{T1}{calligra}{m}{n}

\let\emph\relax % there's no \RedeclareTextFontCommand
\DeclareTextFontCommand{\emph}{\itshape} % Italic instead of default underline

% defs for notation

\DeclareFontFamily{OMX}{MnSymbolE}{}
\DeclareSymbolFont{MnLargeSymbols}{OMX}{MnSymbolE}{m}{n}
\SetSymbolFont{MnLargeSymbols}{bold}{OMX}{MnSymbolE}{b}{n}
\DeclareFontShape{OMX}{MnSymbolE}{m}{n}{
    <-6>  MnSymbolE5
   <6-7>  MnSymbolE6
   <7-8>  MnSymbolE7
   <8-9>  MnSymbolE8
   <9-10> MnSymbolE9
  <10-12> MnSymbolE10
  <12->   MnSymbolE12
}{}
\DeclareFontShape{OMX}{MnSymbolE}{b}{n}{
    <-6>  MnSymbolE-Bold5
   <6-7>  MnSymbolE-Bold6
   <7-8>  MnSymbolE-Bold7
   <8-9>  MnSymbolE-Bold8
   <9-10> MnSymbolE-Bold9
  <10-12> MnSymbolE-Bold10
  <12->   MnSymbolE-Bold12
}{}

\let\llangle\@undefined
\let\rrangle\@undefined
\DeclareMathDelimiter{\llangle}{\mathopen}%
                     {MnLargeSymbols}{'164}{MnLargeSymbols}{'164}
\DeclareMathDelimiter{\rrangle}{\mathclose}%
                     {MnLargeSymbols}{'171}{MnLargeSymbols}{'171}

% Language colour-coding
\def\en#1{{\color{RoyalBlue}\enquote{#1}}}
\def\nl#1{{\color{OrangeRed}\enquote{#1}}}
\def\de#1{{\color{DarkKhaki}\enquote{#1}}}
\def\haw#1{{\color{MediumAquamarine}\enquote{#1}}}
\def\ja#1{{\color{MediumOrchid}\enquote{\begin{CJK*}{UTF8}{min}#1\end{CJK*}}}}

% Fancy highlighting
\def\new#1
{ \highlight{#1} }

\newcommand{\defhighlighter}[3][]{%
  \tikzset{every highlighter/.style={color=#2, fill opacity=#3, #1}}%
}

\defhighlighter{yellow}{.5}

\newcommand{\highlight@DoHighlight}{
  \fill [ decoration = {random steps, amplitude=1pt, segment length=15pt}
        , outer sep = -15pt, inner sep = 0pt, decorate
        , every highlighter, this highlighter ]
        ($(begin highlight)+(0,8pt)$) rectangle ($(end highlight)+(0,-3pt)$) ;
}

\newcommand{\highlight@BeginHighlight}{
  \coordinate (begin highlight) at (0,0) ;
}

\newcommand{\highlight@EndHighlight}{
  \coordinate (end highlight) at (0,0) ;
}

\newdimen\highlight@previous
\newdimen\highlight@current

\DeclareRobustCommand*\highlight[1][]{%
  \tikzset{this highlighter/.style={#1}}%
  \SOUL@setup
  %
  \def\SOUL@preamble{%
    \begin{tikzpicture}[overlay, remember picture]
      \highlight@BeginHighlight
      \highlight@EndHighlight
    \end{tikzpicture}%
  }%
  %
  \def\SOUL@postamble{%
    \begin{tikzpicture}[overlay, remember picture]
      \highlight@EndHighlight
      \highlight@DoHighlight
    \end{tikzpicture}%
  }%
  %
  \def\SOUL@everyhyphen{%
    \discretionary{%
      \SOUL@setkern\SOUL@hyphkern
      \SOUL@sethyphenchar
      \tikz[overlay, remember picture] \highlight@EndHighlight ;%
    }{%
    }{%
      \SOUL@setkern\SOUL@charkern
    }%
  }%
  %
  \def\SOUL@everyexhyphen##1{%
    \SOUL@setkern\SOUL@hyphkern
    \hbox{##1}%
    \discretionary{%
      \tikz[overlay, remember picture] \highlight@EndHighlight ;%
    }{%
    }{%
      \SOUL@setkern\SOUL@charkern
    }%
  }%
  %
  \def\SOUL@everysyllable{%
    \begin{tikzpicture}[overlay, remember picture]
      \path let \p0 = (begin highlight), \p1 = (0,0) in \pgfextra
        \global\highlight@previous=\y0
        \global\highlight@current =\y1
      \endpgfextra (0,0) ;
      \ifdim\highlight@current < \highlight@previous
        \highlight@DoHighlight
        \highlight@BeginHighlight
      \fi
    \end{tikzpicture}%
    \the\SOUL@syllable
    \tikz[overlay, remember picture] \highlight@EndHighlight ;%
  }%
  \SOUL@
}

\def\context#1{{\\[2mm]\footnotesize\itshape #1}}

\usetikzlibrary{calc,trees,positioning,arrows,chains,shapes.geometric,%
    decorations.pathreplacing,decorations.pathmorphing,shapes,%
    matrix,shapes.symbols}

\tikzset{
>=stealth',
  punktchain/.style={
    rectangle,
    rounded corners,
    % fill=black!10,
    draw=black, very thick,
    text width=10em,
    minimum height=3em,
    text centered,
    on chain},
  line/.style={draw, thick, <-},
  element/.style={
    tape,
    top color=white,
    bottom color=blue!50!black!60!,
    minimum width=8em,
    draw=blue!40!black!90, very thick,
    text width=10em,
    minimum height=3.5em,
    text centered,
    on chain},
  every join/.style={->, thick,shorten >=1pt},
  decoration={brace},
  tuborg/.style={decorate},
  tubnode/.style={midway, right=2pt},
}

\bibliographystyle{alphaurl}

\newbool{pd}
\boolfalse{pd}


\newenvironment{milestone}[3]
{ \noindent\begin{minipage}{95mm}\begin{fullwidth}
    \hrule \vspace{2mm}
    { \large\bf Milestone \textsf{#1}: #2 } \hfill { \it\small \ifbool{pd}{PD}{Projected Delivery\global\booltrue{pd}} #3} \\[2mm]
    \hrule \vspace{2mm}
    \noindent Definition of Done:
  \begin{itemize} }
{ \end{itemize}\end{fullwidth}\end{minipage}
  \\[4mm plus 1mm minus 1mm] }

\NewDocumentCommand{\bywhom}{m}{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
   \hfill\mbox{\small\normalfont---\ #1}%
   \parfillskip=0pt \finalhyphendemerits=0 \par}%
}

\definecolor{formalshade}{rgb}{0.98, 0.98, 0.98}
\NewDocumentEnvironment{pquotation}{m}
{  \def\FrameCommand{%
    \hspace{1pt}%
    {\color{Cerulean}\vrule width 1pt}%
    {\color{white}\vrule width 8pt}%
  }%
  \vspace{4pt}\begin{quoting}[
     indentfirst=false,
     leftmargin=\parindent,
     rightmargin=\parindent]
  \MakeFramed{\advance\hsize-\width\FrameRestore}%
  \itshape\small}%
  {\endMakeFramed\bywhom{#1}\end{quoting}\vspace{4pt}}

\newcommand{\lastupdate}
{
  \nottoggle{clean}
  {
\section*{Latest update}
\input{commit.tex}
}{}
}

\newcommand{\todolist}
{ % Generate list of todo items
\nottoggle{clean}
{ \newpage
\section*{ToDo}
\begin{fullwidth}
\begin{itemize}
  \todos{}
\end{itemize}
\end{fullwidth}
}{} }
