\NeedsTeXFormat{LaTeX2e}[1994/06/01]

\ProvidesClass{tufte-book-peikos}[2015/06/30 v3.5.3 Tufte-book class]

%%
% Declare we're tufte-book
\newcommand{\@tufte@class}{book}% the base LaTeX class (defaults to the article/handout style)
\newcommand{\@tufte@pkgname}{tufte-book}% the name of the package (defaults to tufte-handout)

\newcommand{\subtitle}[1]{%
  \gdef\@subtitle{#1}%
}
\newcommand{\maketitlepage}[0]{%
  \cleardoublepage%
  {%
  \rmfamily % previously: \sffamily (sans serif)
  \begin{fullwidth}%
  %make changes to author's name:
 \fontsize{14}{20}\selectfont\par\noindent\textcolor{black}{\allcaps{\thanklessauthor}}%
  \vspace{11.5pc}%
  %make changes to Title:
  \fontsize{27}{36}\selectfont\par\noindent\textcolor{black}{\allcaps{\thanklesstitle}}%
  \vspace{5pc}%
  \\\noindent\parbox{15cm}{\fontsize{20}{24}\selectfont\par\noindent\textcolor{darkgray}{\allcaps{\@subtitle}}}%
  \vfill%
  %make changes to stuff below title
  \fontsize{12}{16}\selectfont\par\noindent\allcaps{\thanklesspublisher}%
  \end{fullwidth}%
  }
  \thispagestyle{empty}%
  \clearpage%
}


%%
% Load the common style elements
\input{tufte-common-peikos.def}


%%
% Set up any book-specific stuff now

%%
% The front matter in Tufte's /Beautiful Evidence/ contains everything up
% to the opening page of Chapter 1.  The running heads, when they appear,
% contain only the (arabic) page number in the outside corner.
%\newif\if@mainmatter \@mainmattertrue
\renewcommand\frontmatter{%
  \if@openright%
    \cleardoublepage%
  \else%
    \clearpage%
  \fi%
  \@mainmatterfalse%
  \pagenumbering{arabic}%
  %\pagestyle{plain}%
  \fancyhf{}%
  \ifthenelse{\boolean{@tufte@twoside}}%
    {\fancyhead[LE,RO]{\thepage}}%
    {\fancyhead[RE,RO]{\thepage}}%
}

%\renewcommand{\thechapter}{\Roman{chapter}}

%%
% The main matter in Tufte's /Beautiful Evidence/ doesn't restart the page
% numbering---it continues where it left off in the front matter.
\renewcommand\mainmatter{%
  \if@openright%
    \cleardoublepage%
  \else%
    \clearpage%
  \fi%
  \@mainmattertrue%
  \fancyhf{}%
  \ifthenelse{\boolean{@tufte@twoside}}%
    {% two-side
      \renewcommand{\chaptermark}[1]{\markboth{##1}{}}%
      \fancyhead[LE]{\thepage\quad\smallcaps{\newlinetospace{\plaintitle}}}% book title
      \fancyhead[RO]{\smallcaps{\newlinetospace{\leftmark}}\quad\thepage}% chapter title
    }%
    {% one-side
      \fancyhead[RE,RO]{\smallcaps{\newlinetospace{\plaintitle}}\quad\thepage}% book title
    }%
}


%%
% The back matter contains appendices, indices, glossaries, endnotes,
% biliographies, list of contributors, illustration credits, etc.
\renewcommand\backmatter{%
  \if@openright%
    \cleardoublepage%
  \else%
    \clearpage%
  \fi%
  \@mainmatterfalse%
}

\let\oldfm\frontmatter
\renewcommand{\frontmatter}{\oldfm\pagenumbering{roman}} % Maybe add \pagestyle{empty}?
\let\oldmm\mainmatter
\renewcommand{\mainmatter}{\oldmm\setcounter{page}{1}\pagenumbering{arabic}}

%%
% Only show the chapter titles in the table of contents
\setcounter{tocdepth}{0}

%%
% If there is a `tufte-book-local.sty' file, load it.

\IfFileExists{tufte-book-local.tex}{%
  \@tufte@info@noline{Loading tufte-book-local.tex}%
  \input{tufte-book-local}%
}{}

%%
% End of file
\endinput
