\documentclass{tufte-handout-peikos}

\title{Goed Gereageerd}

\author{Brian van der Bijl}

\date{April 26, 2021} % without \date command, current date is supplied

%\geometry{showframe} % display margins for debugging page layout

\usepackage{graphicx} % allow embedded images
  \setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
  \graphicspath{{graphics/}} % set of paths to search for images
\usepackage{amsmath}  % extended mathematics
\usepackage{booktabs} % book-quality tables
\usepackage{units}    % non-stacked fractions and better unit spacing
\usepackage{multicol} % multiple column layout facilities
\usepackage{lipsum}   % filler text
\usepackage{csquotes} % fancy quotations
\usepackage{fancyvrb} % extended verbatim environments
  \fvset{fontsize=\normalsize}% default font size for fancy-verbatim environments

% Standardize command font styles and environments
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}}% document class option name
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment

\begin{document}

\maketitle

\begin{abstract}
\noindent
This document contains the first draft for the research-proposal for my master thesis. The primary goal for this iteration of this document is the informed acquisition of a thesis supervisor by providing an outline of the envisioned thesis project. Secondly, this document will provide the basis for the full research-proposal required by the thesis trajectory.
\end{abstract}

\section{Introduction}
Present society is more than ever connected, but simultaneously exceedingly polarised. In the current debate, divisive issues can quickly escalate from minor online discussions to large societal issues. These discussions are often publicly visible, occurring on websites such as Twitter and Facebook, but it has proven to be difficult for ?authorities? to ascertain whether a topic is going to cause a major stir or not.  More critically, it is in this online parlance that the seeds of societal unrest is often instigated. To accurately predict and prevent mobilisation and societal violence, it is crucial for relevant authorities to be able to quickly filter a large amount of communication and direct attention to the cases most likely to turn into problems down the road. Current text-analysis algorithms are ill-equipped to recognise subtext and sentiment in what are often short texts with limited metadata, leading to a reliance on manual monitoring, which lacks the throughput required to adequately and timely deal with issues as they arise.

\section{Organisational Context}
The project \enquote{Goed Gereageerd} is a collaboration between various instances including four research groups of the Utrecht University of Applied Sciences. The thesis research project described in this document will be sponsored by the Research Group for Artificial Intelligence headed by dr. Stefan Leijnen, and will endeavour to find novel solutions to one of the problems identified within \emph{Goed Gereageerd}. Specifically, the issue that will be tackled is a perceived lack of adequacy of the tooling used to identify subtext, sentiment and intention from natural language text, particularly informal text such as online discourse.

\section{Directional Outline}
To address this issue, a number of paths forward have been identified. For this thesis project, the specific intended direction is through the application of formal logic and variations thereupon, combined with more statistical approaches common to modern machine learning. One possible method of pattern recognition that has been suggested involves the application of fuzzy\cite{cintulaFuzzyLogic2017} or many-valued\cite{gottwaldManyValuedLogic2020} logic. In this approach, imprecise or uncertain statements can be formalised and combined whilst retaining the truth functionality of classical logic. Instead of the usual, binary set of truth values (corresponding to set membership or the absence thereof), propositions can be assigned a degree of truth, or a degree of certainty of truth (corresponding to degrees of set membership).
In this proposed application, a finer distinction could be made about whether a scrap of natural language text is considered to be inciting or not. Separate incitement-values could be predicted based on various different indicators, after which these could be aggregated and combined into a norm expressing a degree or probability which will determine how the piece of text should be flagged and whether it should be offered to a human supervisor for inspection. This would also allow for \enquote{soft recommendations}, which can be reinforced or discarded, thus providing a basis for adaptability and learning.

\newthought{A further possibility} for this project that is not yet under consideration is utilising non-monotonicity\cite{strasserNonmonotonicLogic2019} in logic. Non-monotonic captures defeasible inference, where a system or reasoner can draw conclusions which can be retracted if more information becomes available. For example, knowing Tweety is a bird, and birds are generally able to fly, we can reasonably infer that Tweety is in all likelihood able to fly. If further information is added to the system which tells us that Tweety is in fact a flightless bird such as an ostrich, we can safely retract our earlier infererence: whilst \emph{bird} weakly implies \emph{flight}, \emph{ostrich} more strongly implies $\neg$\emph{flight} and thus takes precedence. If more information becomes available containing an even stronger indicator that Tweety can fly, we can retract our earlier retraction, continuously refining our provisional conclusion which represents the best possible prediction based on the knowledge available at the time.
If this approach is applied to the problem of incitement-detection, we can allow the system to make more certain statements about the content of a tweet in the knowledge that these can be retracted if counter-indicators are also detected. For example, as sentence such as \enquote{I'm going to kill ... } should initially be flagged as dangerous. Further text (within the same tweet, or in a follow-up) could point to the recipient of the threat being a roommate, family-member or pet, in which case the earlier text was more likely in hyperbole. This approach could also be tested on a longer term, where the system's attitudes towards an author or organisation could evolve should the target prove more or less problematic than initially anticipated.

% To address this issue, a number of paths forward have been identified. For this thesis project, the specific intended direction is through the application of formal logic and variations thereupon, combined with more statistical approaches common to modern machine learning. One possible method of pattern recognition that has been suggested involves the application of fuzzy or many-valued. A further possibility for this project that is not yet under consideration is utilising non-monotonicity in logic\cite{cintulaFuzzyLogic2017,gottwaldManyValuedLogic2020,strasserNonmonotonicLogic2019}.

\section{Questions}
The main question this research project will address is formulated provisionally as \enquote{in which way can logic-based approaches be utilised to enable and improve the automatic analysis of informal natural text to detect incitement, and to what degree can such an approach improve on further automatisation of currently-existing statistical methods?}. This question will be further refined for the full research-proposal, in which sub-questions will also be added. These sub-questions will likely point towards various proposed solutions as described in the previous section. The primary focus of this research will be targeted at employing aforementioned symbolic techniques towards improving the degree of automisation possible, so as to allow larger scale utilisation and thereby reducing the amount of manual labour required for early detection of incitement. A secondary objective would be to increase the quality of the predictions, facilitating further reduction of human labour requirements by eliminating as many false positives as possible, as well as increasing trustworthiness of the technology by similarly reducing false negatives, thereby limiting the necessity of extra work due to manual cross-verification.

\section{Methods}
This project will be outlined roughly as follows:
\begin{enumerate}
  \item literature review
  \item collection of testing data
  \item implementation of prototypes
  \item evaluation of results
  \item refinement of steps 3-4
  \item final iteration and documentation
  \item comparison to alternative existing methods
\end{enumerate}

\section{Research Limitations}
This project will need to be adequately scoped to avoid chasing a moving horizon. Likely limitations will pertain to the number of approaches compared, which will need to be balanced with the depth any given approach can be explored. The exact bounds of the project will be established in concord with my thesis supervisor and stakeholders for the larger associated project \emph{Goed Gereageerd}.

One limitation on this project will be be the amount of time available. I am under obligation to the Utrecht University of Applied Science to finish my masters degree within reasonable time. The currently envisioned timeline will place most of the work starting as soon as possible, and up to the end of 2021 after which my time to finish up will become more limited. In order to actualise this, I will continue to work on this for the major part of the summer recess.

\bibliography{literature}
\bibliographystyle{plainnat}

%\nocite{*}

\end{document}

